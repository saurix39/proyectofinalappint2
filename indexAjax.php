<script>
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<?php
session_start();

require_once "Logica/Administrador.php";
require_once "Logica/Cliente.php";
require_once "Logica/Doctor.php";
require_once "Logica/Evaluador.php";
require_once "Logica/Cita.php";
require_once "Logica/Accion.php";
require_once "Logica/Log.php";
require_once "Logica/Especialidad.php";

$pid = base64_decode($_GET["pid"]);
include $pid;
?>
