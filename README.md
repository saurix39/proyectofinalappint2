ProyectoFinalAppInt
Este es el repositorio del proyecto QuickMedicine, el cual es un sistema de información
que tiene conexión con base de datos MySql y desarrollado en el lenguaje de programación php.
Este proyecto permite sistematizar procesos relacionados con el area de la medicina en donde un 
cliente solicita una revisión con un medico especialista, de esta manera el sistema se encarga de gestionar el proceso de revision de documentos
y asignación de citas.
En este proyecto se involucran conceptos como ajax, responsive, manejo de formularios, JQuery,
documentos PDF, graficos y estadisticas, trazabilidad de procesos, Bootstrap y bases de datos relacionales. 
