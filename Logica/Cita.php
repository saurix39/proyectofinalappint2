<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/CitaDAO.php";

  class Cita{

    private $idCita;
    private $id_cliente_fk;
    private $id_doctor_fk;
    private $id_peticion_fk;
    private $id_horario_fk;
    private $fecha;
    private $informacion;
    private $estado;
    private $conexion;
    private $citaDAO;

        public function getIdCita(){
            return $this -> idCita;
        }

        public function getId_cliente_fk(){
            return $this -> id_cliente_fk;
        }

        public function getId_doctor_fk(){
            return $this -> id_doctor_fk;
        }

        public function getId_peticion_fk(){
            return $this -> id_peticion_fk;
        }

        public function getId_horario_fk(){
            return $this -> id_horario_fk;
        }

        public function getFecha(){
            return $this -> fecha;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function Cita($idCita="", $id_cliente_fk="", $id_doctor_fk="",$id_peticion_fk="",$id_horario_fk="",$fecha="",$informacion="",$estado=""){
              $this -> idCita = $idCita;
              $this -> id_cliente_fk = $id_cliente_fk;
              $this -> id_doctor_fk = $id_doctor_fk;
              $this -> id_peticion_fk = $id_peticion_fk;
              $this -> id_horario_fk = $id_horario_fk;
              $this -> fecha = $fecha;
              $this -> informacion = $informacion;
              $this -> estado = $estado;
              $this -> conexion = new Conexion();
              $this -> citaDAO = new CitaDAO($this -> idCita, $this -> id_cliente_fk, $this -> id_doctor_fk, $this -> id_peticion_fk, $this -> id_horario_fk, $this -> fecha, $this -> informacion, $this -> estado);
        }

        public function listaCitasOcu($fechaI,$fechaF){
            $this -> conexion-> abrir();
            $this -> conexion-> ejecutar($this -> citaDAO -> listaCitasOcu($fechaI,$fechaF));
            $citas=array();
            while(($resultado = $this -> conexion -> extraer()) != null){
               $cit = new Cita($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],$resultado[7]);
               array_push($citas,$cit);
            }
            $this -> conexion -> cerrar();
            return $citas;
        }

        public function insertar(){
            $this -> conexion-> abrir();
            $this -> conexion-> ejecutar($this -> citaDAO -> insertar());
            $this -> conexion -> cerrar();
        }

        public function listaCitasDeCliente(){
            $this -> conexion-> abrir();
            $this -> conexion-> ejecutar($this -> citaDAO -> listaCitasDeCliente());
            $citas=array();
            while(($resultado = $this -> conexion -> extraer()) != null){
               $cit = new Cita($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],$resultado[7]);
               array_push($citas,$cit);
            }
            $this -> conexion -> cerrar();
            return $citas;
        }

        public function listaCitas(){
            $this -> conexion-> abrir();
            $this -> conexion-> ejecutar($this -> citaDAO -> listaCitas());
            $citas=array();
            while(($resultado = $this -> conexion -> extraer()) != null){
               $cit = new Cita($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],$resultado[7]);
               array_push($citas,$cit);
            }
            $this -> conexion -> cerrar();
            return $citas;
        }

        public function listaCitasDeDoctor($cantidad,$pagina){
            $this -> conexion-> abrir();
            $this -> conexion-> ejecutar($this -> citaDAO -> listaCitasDeDoctor($cantidad,$pagina));
            $citas=array();
            while(($resultado = $this -> conexion -> extraer()) != null){
               $cit = new Cita($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],$resultado[5],$resultado[6],$resultado[7]);
               array_push($citas,$cit);
            }
            $this -> conexion -> cerrar();
            return $citas;
        }

        public function consultarCantidadCitasDoc(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> citaDAO -> consultarCantidadCitasDoc());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function actualizar_Estado_Cit(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> citaDAO -> actualizar_Estado_Cit());
          $this -> conexion -> cerrar();
        }

        public function consultar(){
            $this -> conexion ->abrir();
            $this -> conexion ->ejecutar($this -> citaDAO -> consultar());
            $this -> conexion ->cerrar();
            $resultado= $this -> conexion -> extraer();
            $this -> idCita = $resultado[0];
            $this -> id_cliente_fk = $resultado[1];
            $this -> id_doctor_fk = $resultado[2];
            $this -> id_peticion_fk = $resultado[3];
            $this -> id_horario_fk = $resultado[4];
            $this -> fecha = $resultado[5];
            $this -> informacion = $resultado[6];
            $this -> estado = $resultado[7];
        }

        public function consultarFechasDeCita(){
            $this -> conexion-> abrir();
            $this -> conexion-> ejecutar($this -> citaDAO -> consultarFechasDeCita());
            $citas=array();
            while(($resultado = $this -> conexion -> extraer()) != null){
               $cit = new Cita("","","","","",$resultado[0]);
               array_push($citas,$cit);
            }
            $this -> conexion -> cerrar();
            return $citas;
        }

        public function consultarCantidadDeCitaActPorFecha(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> citaDAO -> consultarCantidadDeCitaActPorFecha());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function consultarCantidadDeCitaRecPorFecha(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> citaDAO -> consultarCantidadDeCitaRecPorFecha());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

  }

?>
