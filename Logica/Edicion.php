<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/EdicionDAO.php";

  class Edicion{

    private $idEdicion;
    private $id_observacion_fk;
    private $informacion;
    private $fecha;
    private $conexion;
    private $edicionDAO;

        public function getIdEdicion(){
            return $this -> idEdicion;
        }

        public function getId_observacion_fk(){
            return $this -> id_observacion_fk;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getFecha(){
        return $this->fecha;
        }

        public function Edicion($idEdicion="", $id_observacion_fk="", $informacion="",$fecha=""){
              $this -> idEdicion = $idEdicion;
              $this -> id_observacion_fk = $id_observacion_fk;
              $this -> informacion = $informacion;
              $this -> fecha = $fecha;
              $this -> conexion = new Conexion();
              $this -> edicionDAO = new EdicionDAO($this -> idEdicion,$this -> id_observacion_fk,$this -> informacion,$this -> fecha);
        }

        public function ediciones(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> edicionDAO -> ediciones());
            $ediciones = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                $edi = new Edicion($resultado[0], $this -> id_observacion_fk, $resultado[1], $resultado[2]);
                array_push($ediciones,$edi);
            }
            $this -> conexion -> cerrar();
            return $ediciones;
        }

        public function registrarEdicion(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> edicionDAO -> registrarEdicion());
            $this -> conexion -> cerrar();
        }

    
  }

?>
