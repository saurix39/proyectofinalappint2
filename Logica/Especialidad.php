<?php
  require_once "Persistencia/Conexion.php";
  require_once "Persistencia/EspecialidadDAO.php";

  class Especialidad{

    private $idEspecialidad;
    private $nombre;
    private $conexion;
    private $especialidadDAO;

        public function getIdEspecialidad(){
            return $this -> idEspecialidad;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function Especialidad($idEspecialidad="",$nombre=""){
            $this -> idEspecialidad = $idEspecialidad;
            $this -> nombre = $nombre;
            $this -> conexion = new Conexion();
            $this -> especialidadDAO = new EspecialidadDAO($this -> idEspecialidad,$this -> nombre);
        }

        public function consultar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> especialidadDAO -> consultar());
            $arrayesp=array();
            while(($resultado = $this -> conexion -> extraer()) !=null){
              $esp = new Especialidad($resultado[0],$resultado[1]);
              array_push($arrayesp,$esp);
            }
            $this -> conexion -> cerrar();
            return $arrayesp;
        }

        public function consultarCantidad($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> especialidadDAO -> consultarCantidad($filtro));
            return $this -> conexion -> extraer();
        }

        public function consultarFiltroPaginacion($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> especialidadDAO -> consultarFiltroPaginacion($filtro,$cantidad,$pagina));
          $esparray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $esp = new Especialidad($resultado[0], $resultado[1]);
            array_push($esparray,$esp);
          }
          $this -> conexion -> cerrar();
          return $esparray;
        }

}
?>
