<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/EdicionPeticionDAO.php";
class EdicionPeticion{
    private $idEdicionPeticion;
    private $id_peticion_fk;
    private $informacion;
    private $remision;
    private $fecha;
    private $edicionPeticionDAO;
    private $conexion;

    public function EdicionPeticion($idEdicionPeticion="",$id_peticion_fk="",$informacion="",$remision="",$fecha=""){
        $this -> idEdicionPeticion = $idEdicionPeticion;
        $this -> id_peticion_fk = $id_peticion_fk;
        $this -> informacion = $informacion;
        $this -> remision = $remision;
        $this -> fecha = $fecha;
        $this -> edicionPeticionDAO = new EdicionPeticionDAO($idEdicionPeticion,$id_peticion_fk,$informacion,$remision,$fecha);
        $this -> conexion = new Conexion();
    }

    public function edicionesPet(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> edicionPeticionDAO -> edicionesPet());
        $ediciones = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $edi = new EdicionPeticion($resultado[0], $this -> id_peticion_fk, $resultado[1], $resultado[2],$resultado[3]);
            array_push($ediciones,$edi);
        }
        $this -> conexion -> cerrar();
        return $ediciones;
    }

    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> edicionPeticionDAO -> insertar());
        $this -> conexion -> cerrar();
    }

    public function getIdEdicionPeticion()
    {
        return $this->idEdicionPeticion;
    }
 
    public function getId_peticion_fk()
    {
        return $this->id_peticion_fk;
    }

    public function getInformacion()
    {
        return $this->informacion;
    }

    public function getRemision()
    {
        return $this->remision;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getEdicionPeticionDAO()
    {
        return $this->edicionPeticionDAO;
    }

    public function getConexion()
    {
        return $this->conexion;
    }
}
?>