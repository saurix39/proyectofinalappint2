<?php
  require_once "Persistencia/Conexion.php";
  require_once "Persistencia/LogDAO.php";

  class Log{

    private $idLog;
    private $idAccion;
    private $idActor;
    private $informacion;
    private $fecha;
    private $conexion;
    private $logDAO;

        public function getIdLog(){
            return $this -> idLog;
        }

        public function getIdAccion(){
            return $this -> idAccion;
        }

        public function getIdActor(){
            return $this -> idActor;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getFecha(){
            return $this ->  fecha;
        }


        public function Log($idLog="",$idAccion="",$idActor="",$informacion="",$fecha=""){
            $this -> idLog = $idLog;
            $this -> idAccion = $idAccion;
            $this -> idActor = $idActor;
            $this -> informacion = $informacion;
            $this -> fecha = $fecha;
            $this -> conexion = new Conexion();
            $this -> logDAO = new LogDAO($this -> idLog,$this -> idAccion,$this -> idActor,$this -> informacion,$this -> fecha);
        }

        public function insertarLogAdministrador(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> insertarLogAdministrador());
            $this -> conexion -> cerrar();
        }

        public function consultarFiltroPaginacionLogAdm($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroPaginacionLogAdm($filtro,$cantidad,$pagina));
          $logarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $log = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($logarray,$log);
          }
          $this -> conexion -> cerrar();
          return $logarray;
        }

        public function consultarCantidadLogAdm($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadLogAdm($filtro));
            return $this -> conexion -> extraer();
        }

        public function consultarLogAdm(){
            $this -> conexion ->abrir();
            $this -> conexion ->ejecutar($this -> logDAO -> consultarLogAdm());
            $this -> conexion ->cerrar();
            $resultado= $this -> conexion -> extraer();
            $this -> id_log = $resultado[0];
            $this -> idAccion = $resultado[1];
            $this -> idActor = $resultado[2];
            $this -> informacion =$resultado[3];
            $this -> fecha =$resultado[4];
        }

        public function insertarLogDoctor(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> insertarLogDoctor());
            $this -> conexion -> cerrar();
        }

        public function consultarFiltroPaginacionLogDoc($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroPaginacionLogDoc($filtro,$cantidad,$pagina));
          $logarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $log = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($logarray,$log);
          }
          $this -> conexion -> cerrar();
          return $logarray;
        }

        public function consultarCantidadLogDoc($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadLogDoc($filtro));
            return $this -> conexion -> extraer();
        }

        public function consultarLogDoc(){
            $this -> conexion ->abrir();
            $this -> conexion ->ejecutar($this -> logDAO -> consultarLogDoc());
            $this -> conexion ->cerrar();
            $resultado= $this -> conexion -> extraer();
            $this -> id_log = $resultado[0];
            $this -> idAccion = $resultado[1];
            $this -> idActor = $resultado[2];
            $this -> informacion =$resultado[3];
            $this -> fecha =$resultado[4];
        }

        public function insertarLogCliente(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> insertarLogCliente());
            $this -> conexion -> cerrar();
        }

        public function consultarFiltroPaginacionLogCli($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroPaginacionLogCli($filtro,$cantidad,$pagina));
          $logarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $log = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($logarray,$log);
          }
          $this -> conexion -> cerrar();
          return $logarray;
        }

        public function consultarCantidadLogCli($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadLogCli($filtro));
            return $this -> conexion -> extraer();
        }

        public function consultarLogCli(){
            $this -> conexion ->abrir();
            $this -> conexion ->ejecutar($this -> logDAO -> consultarLogCli());
            $this -> conexion ->cerrar();
            $resultado= $this -> conexion -> extraer();
            $this -> id_log = $resultado[0];
            $this -> idAccion = $resultado[1];
            $this -> idActor = $resultado[2];
            $this -> informacion =$resultado[3];
            $this -> fecha =$resultado[4];
        }

        public function insertarLogEvaluador(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> insertarLogEvaluador());
            $this -> conexion -> cerrar();
        }

        public function consultarFiltroPaginacionLogEva($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroPaginacionLogEva($filtro,$cantidad,$pagina));
          $logarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $log = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($logarray,$log);
          }
          $this -> conexion -> cerrar();
          return $logarray;
        }

        public function consultarCantidadLogEva($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidadLogEva($filtro));
            return $this -> conexion -> extraer();
        }

        public function consultarLogEva(){
            $this -> conexion ->abrir();
            $this -> conexion ->ejecutar($this -> logDAO -> consultarLogEva());
            $this -> conexion ->cerrar();
            $resultado= $this -> conexion -> extraer();
            $this -> id_log = $resultado[0];
            $this -> idAccion = $resultado[1];
            $this -> idActor = $resultado[2];
            $this -> informacion =$resultado[3];
            $this -> fecha =$resultado[4];
        }
}
?>
