<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/DoctorDAO.php";

  class Doctor{

    private $idDoctor;
    private $cedula;
    private $correo;
    private $clave;
    private $especialidad;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;
    private $id_turno_fk;
    private $conexion;
    private $doctorDAO;

        public function getIdDoctor(){
            return $this -> idDoctor;
        }

        public function getCedula(){
            return $this -> cedula;
        }

        public function getCorreo(){
            return $this -> correo;
        }

        public function getClave(){
            return $this -> clave;
        }

        public function getEspecialidad(){
            return $this -> especialidad;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getApellido(){
            return $this -> apellido;
        }

        public function getFech_nac(){
            return $this -> fech_nac;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function getFoto(){
            return $this -> foto;
        }

        public function getId_turno_fk(){
            return $this -> id_turno_fk;
        }

        public function Doctor($idDoctor="",$cedula="",$correo="",$clave="",$especialidad="",$nombre="",$apellido="",$fech_nac="",$estado="",$foto="", $id_turno_fk=""){
              $this -> idDoctor = $idDoctor;
              $this -> cedula = $cedula;
              $this -> correo = $correo;
              $this -> clave = $clave;
              $this -> especialidad = $especialidad;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> fech_nac = $fech_nac;
              $this -> estado = $estado;
              $this -> foto = $foto;
              $this -> id_turno_fk = $id_turno_fk;
              $this -> conexion = new Conexion();
              $this -> doctorDAO = new DoctorDAO($this -> idDoctor, $this -> cedula, $this -> correo, $this -> clave, $this -> especialidad, $this -> nombre,  $this -> apellido, $this -> fech_nac, $this -> estado, $this -> foto, $this -> id_turno_fk);
        }
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()==1){
            $resultado=$this -> conexion -> extraer();
            $this -> idDoctor = $resultado[0];
            $this -> estado = $resultado[1];
            return 1;
        }else{
            return 0;
        }
    }

    public function consultar(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> doctorDAO -> consultar());
        $this -> conexion ->cerrar();
        $resultado= $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto=$resultado[3];
        $this -> cedula=$resultado[4];
        $this -> fech_nac=$resultado[5];
        $this -> especialidad=$resultado[6];
        $this -> estado=$resultado[7];
        $this -> id_turno_fk=$resultado[8];
    }

    public function obtenerNombres(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> obtenerNombres());
        $nombres=array();
        while(($resultado = $this -> conexion -> extraer()) !=null){
            array_push($nombres,$resultado[0]);
        }
        $this -> conexion -> cerrar();
        return $nombres;
    }

    public function consultarIdNombre($nombre){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> consultarIdNombre($nombre));
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function consultarNombreId($id_esp){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> consultarNombreId($id_esp));
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function registrarDoctor(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> registrarDoctor());
        $this -> conexion -> cerrar();
    }

    public function actualizarInformacion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> doctorDAO -> actualizarInformacion());
        $this -> conexion -> cerrar();
    }


        public function consultarDoctores(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> doctorDAO -> consultarDoctores());
          $docarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $doc = new Doctor($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9]);
            array_push($docarray,$doc);
          }
          $this -> conexion -> cerrar();
          return $docarray;
        }

        public function consultarFiltroPaginacion($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> doctorDAO -> consultarFiltroPaginacion($filtro,$cantidad,$pagina));
          $docarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $doc = new Doctor($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9]);
            array_push($docarray,$doc);
          }
          $this -> conexion -> cerrar();
          return $docarray;
        }


        public function consultarCantidad($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> consultarCantidad($filtro));
            return $this -> conexion -> extraer();
        }

        public function actualizar_Estado_Doc(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> doctorDAO -> actualizar_Estado_Doc());
          $this -> conexion -> cerrar();
        }

        public function existeEspecialidad($nombre){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> existeEspecialidad($nombre));
            $this -> conexion -> cerrar();
            if($this -> conexion -> numFilas()>=1){
                return 1;
            }else{
                return 0;
            }
        }

        public function registrarEspecialidad($nombre){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> registrarEspecialidad($nombre));
            $this -> conexion -> cerrar();
        }

        public function listaDoc(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> listaDoc());
            $doctores = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $doc = new Doctor($resultado[0], $resultado[1], $resultado[2], $resultado[3], $this -> especialidad,$resultado[4],$resultado[5],$resultado[6],"1",$resultado[7],$resultado[8]);
              array_push($doctores,$doc);
            }
            $this -> conexion -> cerrar();
            return $doctores;
        }

        public function consultarDoctoresEspecialidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> consultarDoctoresEspecialidad());
            $doctores = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $doc = new Doctor($resultado[0], $resultado[1], $resultado[2], $resultado[3], $this -> especialidad,$resultado[4],$resultado[5],$resultado[6],"",$resultado[7],$resultado[8]);
              array_push($doctores,$doc);
            }
            $this -> conexion -> cerrar();
            return $doctores;
        }

        public function consultarAjax(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> consultarAjax());
            $this -> conexion -> cerrar();
            $resultado = $this -> conexion -> extraer();
            $this -> cedula = $resultado[0];
            $this -> correo = $resultado[1];
            $this -> especialidad = $resultado[2];
            $this -> nombre = $resultado[3];
            $this -> apellido = $resultado[4];
            $this -> fech_nac = $resultado[5];
            $this -> estado = "1";
            $this -> foto = $resultado[6];
            $this -> id_turno_fk = $resultado[7];
        }

        public function consultarCantidadEspecialidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> doctorDAO -> consultarCantidadEspecialidad());
            return $this -> conexion -> extraer();
        }

  }

?>
