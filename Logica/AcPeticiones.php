<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/AcPeticionesDAO.php";
class AcPeticiones{
    private $idAc;
    private $id_peticion_fk;
    private $id_evaluador_fk;
    private $id_accion_fk;
    private $fecha;
    private $acPeticionesDAO;
    private $conexion;


    public function AcPeticiones($idAc="",$id_peticion_fk="",$id_evaluador_fk="",$id_accion_fk="",$fecha=""){
        $this -> idAc = $idAc;
        $this -> id_peticion_fk = $id_peticion_fk;
        $this -> id_evaluador_fk = $id_evaluador_fk;
        $this -> id_accion_fk = $id_accion_fk;
        $this -> fecha = $fecha;
        $this -> acPeticionesDAO = new AcPeticionesDAO($idAc,$id_peticion_fk,$id_evaluador_fk,$id_accion_fk,$fecha);
        $this -> conexion = new Conexion();
    }

    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> acPeticionesDAO -> insertar());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> acPeticionesDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idAc= $resultado[0];
        $this -> id_evaluador_fk= $resultado[1];
        $this -> fecha= $resultado[2];
    }

    public function getIdAc()
    {
        return $this->idAc;
    }

    public function getId_peticion_fk()
    {
        return $this->id_peticion_fk;
    }

    public function getId_evaluador_fk()
    {
        return $this->id_evaluador_fk;
    }

    public function getId_accion_fk()
    {
        return $this->id_accion_fk;
    }

    public function getFecha()
    {
        return $this->fecha;
    }
}
?>