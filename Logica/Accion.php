<?php
  require_once "Persistencia/Conexion.php";
  require_once "Persistencia/AccionDAO.php";

  class Accion{

    private $idAccion;
    private $nombre;
    private $conexion;
    private $accionDAO;

        public function getIdAccion(){
            return $this -> idAccion;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function Accion($idAccion="",$nombre=""){
            $this -> idAccion = $idAccion;
            $this -> nombre = $nombre;
            $this -> conexion = new Conexion();
            $this -> accionDAO = new AccionDAO($this -> idAccion,$this -> nombre);
        }

        public function consultarAcciones(){
          $this -> conexion ->abrir();
          $this -> conexion ->ejecutar($this -> accionDAO -> consultarAcciones());
          $this -> conexion ->cerrar();
              $resultado = $this -> conexion -> extraer();
              $this -> idAccion = $resultado[0];
              $this -> nombre = $resultado[1];
        }

        public function consultarNombresAcciones(){
          $this -> conexion ->abrir();
          $this -> conexion ->ejecutar($this -> accionDAO -> consultarNombresAcciones());
          $this -> conexion ->cerrar();
          $resultado = $this -> conexion -> extraer();
          $this -> nombre = $resultado[0];
        }

}
?>
