<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/AdministradorDAO.php";
class Administrador{

    private $idAdministrador;
    private $cedula;
    private $correo;
    private $clave;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;
    private $conexion;
    private $administradorDAO;

        public function getIdAdministrador(){
            return $this -> idAdministrador;
        }

        public function getCedula(){
            return $this -> cedula;
        }

        public function getCorreo(){
            return $this -> correo;
        }

        public function getClave(){
            return $this -> clave;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getApellido(){
            return $this -> apellido;
        }

        public function getFech_nac(){
            return $this -> fech_nac;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function getFoto(){
            return $this -> foto;
        }

        public function Administrador($idAdministrador="", $cedula="", $correo="",$clave="",$nombre="",$apellido="",$fech_nac="",$estado="",$foto=""){
              $this -> idAdministrador = $idAdministrador;
              $this -> cedula = $cedula;
              $this -> correo = $correo;
              $this -> clave = $clave;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> fech_nac = $fech_nac;
              $this -> estado = $estado;
              $this -> foto = $foto;
              $this -> conexion = new Conexion();
              $this -> administradorDAO = new AdministradorDAO($this -> idAdministrador, $this -> cedula, $this -> correo, $this -> clave, $this -> nombre,  $this -> apellido, $this -> fech_nac, $this -> estado, $this -> foto);

        }

        public function autenticar(){
          $this -> conexion ->abrir();
          $this -> conexion ->ejecutar($this -> administradorDAO -> autenticar());
          $this -> conexion ->cerrar();
          if($this -> conexion -> numFilas()==1){
              $resultado = $this -> conexion -> extraer();
              $this -> idAdministrador = $resultado[0];
              $this -> estado = $resultado[1];
              return 1;
          }else{
              return 0;
          }
        }

        public function consultarAdministradores(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> administradorDAO -> consultarAdministradores());
          $admarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $adm = new Administrador($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($admarray,$adm);
          }
          $this -> conexion -> cerrar();
          return $admarray;

        }

        public function consultarFiltroPaginacion($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> administradorDAO -> consultarFiltroPaginacion($filtro,$cantidad,$pagina));
          $admarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $adm = new Administrador($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($admarray,$adm);
          }
          $this -> conexion -> cerrar();
          return $admarray;
        }

    public function consultar(){
        $this -> conexion ->abrir();
        $this -> conexion ->ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion ->cerrar();
        $resultado= $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto=$resultado[3];
        $this -> cedula=$resultado[4];
        $this -> fech_nac=$resultado[5];
        $this -> estado=$resultado[6];
    }

    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas()>=1){
            return 1;
        }else{
            return 0;
        }
    }

    public function registrarAdministrador(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> registrarAdministrador());
        $this -> conexion -> cerrar();
    }

    public function actualizarInformacion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> actualizarInformacion());
        $this -> conexion -> cerrar();
    }



        public function consultarCantidad($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> administradorDAO -> consultarCantidad($filtro));
            return $this -> conexion -> extraer();
        }

        public function actualizar_Estado_Adm(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> administradorDAO -> actualizar_Estado_Adm());
          $this -> conexion -> cerrar();
      }
}
