<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/PeticionDAO.php";
require_once "Logica/Cliente.php";


  class Peticion{

    private $idPeticion;
    private $id_evaluador_fk;
    private $id_cliente_fk;
    private $especialidad;
    private $informacion;
    private $remision;
    private $fecha;
    private $estado;
    private $conexion;
    private $peticionDAO;

        public function getIdPeticion(){
            return $this -> idPeticion;
        }

        public function getId_evaluador_fk(){
            return $this -> id_evaluador_fk;
        }

        public function getId_cliente_fk(){
            return $this -> id_cliente_fk;
        }

        public function getEspecialidad(){
            return $this -> especialidad;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function getRemision(){
            return $this-> remision;
        }

        public function getFecha(){
            return $this-> fecha;
        }

        public function Peticion($idPeticion="",$id_evaluador_fk="",$id_cliente_fk="",$especialidad="",$informacion="",$remision="",$fecha="",$estado=""){
              $this -> idPeticion = $idPeticion;
              $this -> id_evaluador_fk = $id_evaluador_fk;
              $this -> id_cliente_fk = $id_cliente_fk;
              $this -> especialidad = $especialidad;
              $this -> informacion = $informacion;
              $this -> remision = $remision;
              $this -> fecha = $fecha;
              $this -> estado = $estado;
              $this -> conexion = new Conexion();
              $this -> peticionDAO = new PeticionDAO($this -> idPeticion, $this -> id_evaluador_fk, $this -> id_cliente_fk, $this -> especialidad, $this -> informacion, $this -> remision,$this -> fecha,$this -> estado);
        }

        public function asignarEvaluador(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> asignarEvaluador());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function registrarPeticion(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> registrarPeticion());
            $this -> conexion -> cerrar();
        }

        public function consultarPeticiones(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticiones());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($resultado[1],$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0], "", $cli, $resultado[2], "","",$resultado[3],0);
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultarPeticionesPorCo(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticionesPorCo());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($resultado[1],$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0], "", $cli, $resultado[2], "","",$resultado[3],0);
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultarPeticionesCo(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticionesCo());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($resultado[1],$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0], "", $cli, $resultado[2], "","",$resultado[3],0);
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultar());
            $this -> conexion -> cerrar();
            $resultado = $this -> conexion -> extraer();
            $cli= new Cliente($resultado[0],$resultado[1],$resultado[2],"",$resultado[3],$resultado[4],$resultado[5],"1",$resultado[6]);
            $evaluador= new Evaluador($resultado[7],$resultado[8],$resultado[9],"",$resultado[10],$resultado[11],$resultado[12],"1",$resultado[13]);
            $this -> id_evaluador_fk=$evaluador;
            $this -> id_cliente_fk=$cli;
            $this -> especialidad=$resultado[14];
            $this -> informacion=$resultado[15];
            $this -> remision=$resultado[16];
            $this -> fecha=$resultado[17];
            $this -> estado=$resultado[18];
        }

        public function consultarInfo(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarInfo());
            $this -> conexion -> cerrar();
            $resultado = $this -> conexion -> extraer();
            $cli= new Cliente($resultado[0],$resultado[1],$resultado[2],"",$resultado[3],$resultado[4],$resultado[5],"1",$resultado[6]);
            $evaluador= new Evaluador($resultado[7],$resultado[8],$resultado[9],"",$resultado[10],$resultado[11],$resultado[12],"1",$resultado[13]);
            $this -> id_evaluador_fk=$evaluador;
            $this -> id_cliente_fk=$cli;
            $this -> especialidad=$resultado[14];
            $this -> informacion=$resultado[15];
            $this -> remision=$resultado[16];
            $this -> fecha=$resultado[17];
            $this -> estado=$resultado[18];
        }

        public function negar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> negar());
            $this -> conexion -> cerrar();
        }

        public function aceptar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> aceptar());
            $this -> conexion -> cerrar();
        }

        public function corregir(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> corregir());
            $this -> conexion -> cerrar();
        }

        public function enObser(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> enObser());
            $this -> conexion -> cerrar();
        }

        public function concretar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> concretar());
            $this -> conexion -> cerrar();
        }

        public function consultarPeticionesPorCoC(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticionesPorCoC());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($this -> id_cliente_fk,$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0], $resultado[1], $cli, $resultado[2], "","",$resultado[3],"-2");
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultarPeticionesAceptadas(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticionesAceptadas());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($resultado[1],$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0],$resultado[6], $cli, $resultado[2], "","",$resultado[3],0);
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultarPeticionesRechazadasCli(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticionesRechazadasCli());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($resultado[1],$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0],$resultado[6], $cli, $resultado[2], "","",$resultado[3],0);
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultarPeticionesRechazadasEva(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarPeticionesRechazadasEva());
            $petarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $cli= new Cliente($resultado[1],$resultado[4],"","",$resultado[5]);
              $pet = new Peticion($resultado[0],$resultado[6], $cli, $resultado[2], "","",$resultado[3],0);
              array_push($petarray,$pet);
            }
            $this -> conexion -> cerrar();
            return $petarray;
        }

        public function consultarCantidadPeticionesPorEstado(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> peticionDAO -> consultarCantidadPeticionesPorEstado());
            return $this -> conexion -> extraer();
        }

  }
?>
