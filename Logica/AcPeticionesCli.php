<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/AcPeticionesCliDAO.php";
class AcPeticionesCli{
    private $idAc;
    private $id_peticion_fk;
    private $id_cliente_fk;
    private $id_accion_fk;
    private $fecha;
    private $acPeticionesCliDAO;
    private $conexion;


    public function AcPeticionesCli($idAc="",$id_peticion_fk="",$id_cliente_fk="",$id_accion_fk="",$fecha=""){
        $this -> idAc = $idAc;
        $this -> id_peticion_fk = $id_peticion_fk;
        $this -> id_cliente_fk = $id_cliente_fk;
        $this -> id_accion_fk = $id_accion_fk;
        $this -> fecha = $fecha;
        $this -> acPeticionesCliDAO = new AcPeticionesCliDAO($idAc,$id_peticion_fk,$id_cliente_fk,$id_accion_fk,$fecha);
        $this -> conexion = new Conexion();
    }

    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> acPeticionesCliDAO -> insertar());
        $this -> conexion -> cerrar();
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> acPeticionesCliDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idAc= $resultado[0];
        $this -> id_cliente_fk= $resultado[1];
        $this -> fecha= $resultado[2];
    }

    public function getIdAc()
    {
        return $this->idAc;
    }

    public function getId_peticion_fk()
    {
        return $this->id_peticion_fk;
    }

    public function getId_cliente_fk()
    {
        return $this->id_cliente_fk;
    }

    public function getId_accion_fk()
    {
        return $this->id_accion_fk;
    }

    public function getFecha()
    {
        return $this->fecha;
    }
}
?>