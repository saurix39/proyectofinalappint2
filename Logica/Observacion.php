<?php
require_once "Persistencia/Conexion.php";
require_once "Persistencia/ObservacionDAO.php";


  class Observacion{

    private $idObservacion;
    private $id_peticion_fk;
    private $informacion;
    private $fecha;
    private $conexion;
    private $observacionDAO;

        public function getIdObservacion(){
            return $this -> idObservacion;
        }

        public function getId_peticion_fk(){
            return $this -> id_peticion_fk;
        }

        public function getInformacion(){
            return $this -> informacion;
        }

        public function getFecha()
        {
        return $this->fecha;
        }

        public function Observacion($idObservacion="", $id_peticion_fk="", $informacion="", $fecha=""){
              $this -> idObservacion = $idObservacion;
              $this -> id_peticion_fk = $id_peticion_fk;
              $this -> informacion = $informacion;
              $this -> fecha = $fecha;
              $this -> conexion = new Conexion();
              $this -> observacionDAO = new ObservacionDAO($this -> idObservacion,$this -> id_peticion_fk,$this -> informacion,$this -> fecha);
        }

        public function observaciones(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> observacionDAO -> observaciones());
            $observaciones = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
                $obs = new Observacion($resultado[0], $this -> id_peticion_fk, $resultado[1], $resultado[2]);
                array_push($observaciones,$obs);
            }
            $this -> conexion -> cerrar();
            return $observaciones;
        }

        public function registrarObservacion(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> observacionDAO -> registrarObservacion());
            $this -> conexion -> cerrar();
        }


    
  }

?>
