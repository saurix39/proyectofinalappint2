<?php
  class EspecialidadDAO{

    private $idEspecialidad;
    private $nombre;

        public function EspecialidadDAO($idEspecialidad="",$nombre=""){
            $this -> idEspecialidad = $idEspecialidad;
            $this -> nombre = $nombre;
        }

        public function consultar(){
          return "select id_especialidad, nombre_especialidad from especialidad";
        }

        public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
              return "select id_especialidad, nombre_especialidad
                      from especialidad
                      where nombre_especialidad like '%" . $filtro . "%' limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidad($filtro){
          return "select count(id_especialidad)
                  from especialidad
                  where nombre_especialidad like '%".$filtro."%'";
        }
}
?>
