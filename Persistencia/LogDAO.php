<?php

  class LogDAO{

    private $idLog;
    private $idAccion;
    private $idActor;
    private $informacion;
    private $fecha;


        public function LogDAO($idLog="",$idAccion="",$idActor="",$informacion="",$fecha=""){
            $this -> idLog = $idLog;
            $this -> idAccion = $idAccion;
            $this -> idActor = $idActor;
            $this -> informacion = $informacion;
            $this -> fecha = $fecha;
        }

        public function insertarLogAdministrador(){
            return "insert into logadministrador (id_accion_fk,id_administrador_fk,informacion,fecha_hora)
                    values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
        }

        public function consultarFiltroPaginacionLogAdm($filtro,$cantidad, $pagina){
              return "select id_log,id_accion_fk,id_administrador_fk, informacion, fecha_hora
                      from logadministrador inner join accion a on (id_accion_fk=id_accion)
                      where a.nombre_accion like '%" . $filtro . "%' order by id_log desc limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidadLogAdm($filtro){
          return "select count(id_log)
                  from logadministrador inner join accion a on (id_accion_fk=id_accion)
                  where a.nombre_accion like '%" . $filtro."%'";
        }

        public function consultarLogAdm(){
              return "select id_log,id_accion_fk,id_administrador_fk, informacion, fecha_hora
                      from logadministrador
                      where id_log = '".$this -> idLog."'";
        }

        public function insertarLogDoctor(){
            return "insert into logdoctor (id_accion_fk,id_doctor_fk,informacion,fecha_hora)
                    values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
        }

        public function consultarFiltroPaginacionLogDoc($filtro,$cantidad, $pagina){
              return "select id_log,id_accion_fk,id_doctor_fk, informacion, fecha_hora
                      from logdoctor inner join accion a on (id_accion_fk=id_accion)
                      where a.nombre_accion like '%" . $filtro . "%' order by id_log desc limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidadLogDoc($filtro){
          return "select count(id_log)
                  from logdoctor inner join accion a on (id_accion_fk=id_accion)
                  where a.nombre_accion like '%" . $filtro."%'";
        }

        public function consultarLogDoc(){
              return "select id_log,id_accion_fk,id_doctor_fk, informacion, fecha_hora
                      from logdoctor
                      where id_log = '".$this -> idLog."'";
        }

        public function insertarLogCliente(){
            return "insert into logcliente (id_accion_fk,id_cliente_fk,informacion,fecha_hora)
                    values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
        }

        public function consultarFiltroPaginacionLogCli($filtro,$cantidad, $pagina){
              return "select id_log,id_accion_fk,id_cliente_fk, informacion, fecha_hora
                      from logcliente inner join accion a on (id_accion_fk=id_accion)
                      where a.nombre_accion like '%" . $filtro . "%' order by id_log desc limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidadLogCli($filtro){
          return "select count(id_log)
                  from logcliente inner join accion a on (id_accion_fk=id_accion)
                  where a.nombre_accion like '%" . $filtro."%'";
        }

        public function consultarLogCli(){
              return "select id_log,id_accion_fk,id_cliente_fk, informacion, fecha_hora
                      from logcliente
                      where id_log = '".$this -> idLog."'";
        }

        public function insertarLogEvaluador(){
            return "insert into logevaluador (id_accion_fk,	id_evaluador_fk,informacion,fecha_hora)
                    values ('". $this -> idAccion ."','". $this -> idActor ."','". $this -> informacion ."','". $this -> fecha ."')";
        }

        public function consultarFiltroPaginacionLogEva($filtro,$cantidad, $pagina){
              return "select id_log,id_accion_fk,	id_evaluador_fk, informacion, fecha_hora
                      from logevaluador inner join accion a on (id_accion_fk=id_accion)
                      where a.nombre_accion like '%" . $filtro . "%' order by id_log desc limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidadLogEva($filtro){
          return "select count(id_log)
                  from logevaluador inner join accion a on (id_accion_fk=id_accion)
                  where a.nombre_accion like '%" . $filtro."%'";
        }

        public function consultarLogEva(){
              return "select id_log,id_accion_fk,	id_evaluador_fk, informacion, fecha_hora
                      from logevaluador
                      where id_log = '".$this -> idLog."'";
        }
}
?>
