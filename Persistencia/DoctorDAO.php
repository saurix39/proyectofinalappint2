<?php
  class DoctorDAO{

    private $idDoctor;
    private $cedula;
    private $correo;
    private $clave;
    private $especialidad;
    private $nombre;
    private $apellido;
    private $fech_nac;
    private $estado;
    private $foto;
    private $id_turno_fk;

        public function DoctorDAO($idDoctor="", $cedula="", $correo="",$clave="", $especialidad="" ,$nombre="",$apellido="",$fech_nac="",$estado="",$foto="", $id_turno_fk=""){
              $this -> idDoctor = $idDoctor;
              $this -> cedula = $cedula;
              $this -> correo = $correo;
              $this -> clave = $clave;
              $this -> especialidad = $especialidad;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> fech_nac = $fech_nac;
              $this -> estado = $estado;
              $this -> foto = $foto;
              $this -> id_turno_fk = $id_turno_fk;
        }
      public function autenticar(){
        return "select id_doctor, estado from doctor where correo='". $this -> correo ."' and clave='". md5($this -> clave) ."'";
      }

      public function consultar(){
        return "select nombre, apellido, correo, foto, cedula, fech_nac, e.nombre_especialidad,estado,id_turno_fk
                from doctor
                inner join especialidad e on (id_especialidad=id_especialidad_fk)
                where id_doctor='". $this -> idDoctor ."'";
      }

      public function obtenerNombres(){
        return "select nombre_especialidad from especialidad";
      }

      public function consultarIdNombre($nombre){
        return "select id_especialidad from especialidad where nombre_especialidad='". $nombre ."'";
      }


      public function consultarNombreId($id_esp){
        return "select nombre_especialidad from especialidad where id_especialidad='". $id_esp ."'";
      }

      public function existeCorreo(){
        return "select correo from doctor where correo='". $this -> correo ."'";
      }

      public function registrarDoctor(){
        return "insert into doctor (cedula,nombre,apellido,correo,clave,foto,fech_nac,estado,id_especialidad_fk,id_turno_fk) values ('". $this -> cedula ."','". $this -> nombre ."','". $this -> apellido ."','". $this -> correo ."','". md5($this -> clave) ."','". (($this -> foto != "")?$this -> foto:"") ."','". $this -> fech_nac ."','1','". $this -> especialidad ."','". $this -> id_turno_fk ."')";
      }

      public function actualizarInformacion(){
        return "update doctor set cedula='". $this -> cedula ."', nombre='". $this -> nombre ."', apellido='". $this -> apellido ."', fech_nac='". $this -> fech_nac ."', foto='". $this -> foto ."', id_especialidad_fk='". $this -> especialidad ."', id_turno_fk='". $this -> id_turno_fk ."' where id_doctor='". $this -> idDoctor ."'";
      }



        public function consultarDoctores(){
          return "select id_doctor,cedula,correo,id_especialidad_fk,nombre,apellido,fech_nac,estado,foto,id_turno_fk
                  from doctor";
        }

        public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
          return "select id_doctor,cedula,correo,e.nombre_especialidad,nombre,apellido,fech_nac,estado,foto,id_turno_fk
                  from doctor
                    inner join especialidad e on (id_especialidad=id_especialidad_fk)
                  where cedula like '%" . $filtro . "%' limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidad($filtro){
          return "select count(id_doctor)
                  from doctor
                  where cedula like '%".$filtro."%'";
        }

        public function actualizar_Estado_Doc(){
          return "update doctor
                  set estado = '" . $this -> estado. "'
                  where id_doctor = '" . $this -> idDoctor ."'";
        }

        public function existeEspecialidad($nombre){
          return "select id_especialidad from especialidad where nombre_especialidad='".$nombre."'";
        }

        public function registrarEspecialidad($nombre){
          return "insert into especialidad (nombre_especialidad) values ('".$nombre."')";
        }

        public function listaDoc(){
          return "select id_doctor,cedula,correo,clave,nombre,apellido,fech_nac,foto,id_turno_fk from doctor where id_especialidad_fk='". $this -> especialidad ."' and estado='1'";
        }

        public function consultarDoctoresEspecialidad(){
          return "select id_doctor,cedula,correo,clave,nombre,apellido,fech_nac,foto,id_turno_fk from doctor where id_especialidad_fk='". $this -> especialidad ."'";
        }

        public function consultarAjax(){
          return "select cedula,correo,id_especialidad_fk,nombre,apellido,fech_nac,foto,id_turno_fk from doctor where id_doctor='". $this -> idDoctor ."' and estado='1'";
        }

        public function consultarCantidadEspecialidad(){
          return "select count(*) from doctor where id_especialidad_fk='". $this -> especialidad ."'";
        }
  }

?>
