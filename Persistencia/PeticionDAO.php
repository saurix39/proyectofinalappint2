<?php

  class PeticionDAO{

    private $idPeticion;
    private $id_evaluador_fk;
    private $id_cliente_fk;
    private $especialidad;
    private $informacion;
    private $remision;
    private $fecha;
    private $estado;

        public function PeticionDAO($idPeticion="",$id_evaluador_fk="",$id_cliente_fk="",$especialidad="",$informacion="",$remision="",$fecha="",$estado=""){
              $this -> idPeticion = $idPeticion;
              $this -> id_evaluador_fk = $id_evaluador_fk;
              $this -> id_cliente_fk = $id_cliente_fk;
              $this -> especialidad = $especialidad;
              $this -> informacion = $informacion;
              $this -> remision = $remision;
              $this -> fecha = $fecha;
              $this -> estado = $estado;
        }

        public function asignarEvaluador(){
          return "select c.eva, c.cant from (select id_evaluador eva, count(id_evaluador) as cant from peticion p right OUTER JOIN evaluador e on (id_evaluador_fk=id_evaluador) where e.estado=1 group by (id_evaluador) order by(cant) asc) as c limit 1";
        }

        public function registrarPeticion(){
          return "insert into peticion (id_evaluador_fk,id_cliente_fk,especialidad,informacion,remision,estado,fecha)
                  values('". $this -> id_evaluador_fk ."','". $this -> id_cliente_fk ."','". $this -> especialidad ."','". $this -> informacion ."','". $this -> remision ."','". $this -> estado ."','".$this -> fecha."')";
        }

        public function consultarPeticiones(){
          return "select id_peticion, id_cliente_fk, nombre_especialidad, fecha,cedula, nombre from peticion inner join cliente on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad) where id_evaluador_fk='". $this -> id_evaluador_fk ."' and peticion.estado='0' and cliente.estado='1'";
        }

        public function consultarPeticionesPorCo(){
          return "select id_peticion, id_cliente_fk, nombre_especialidad, fecha,cedula, nombre from peticion inner join cliente on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad) where id_evaluador_fk='". $this -> id_evaluador_fk ."' and peticion.estado='-2' and cliente.estado='1'";
        }

        public function consultarPeticionesCo(){
          return "select id_peticion, id_cliente_fk, nombre_especialidad, fecha,cedula, nombre from peticion inner join cliente on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad) where id_evaluador_fk='". $this -> id_evaluador_fk ."' and peticion.estado='2' and cliente.estado='1'";
        }

        public function consultar(){
          return "select id_cliente_fk,c.cedula,c.correo,c.nombre,c.apellido,c.fech_nac,c.foto, id_evaluador_fk,e.cedula,e.correo,e.nombre,e.apellido,e.fech_nac,e.foto,nombre_especialidad, informacion,remision, fecha, peticion.estado from peticion inner join cliente c on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad) inner join evaluador e on (id_evaluador_fk=id_evaluador) where id_peticion='". $this -> idPeticion ."'";
        }

        public function consultarInfo(){
          return "select id_cliente_fk,c.cedula,c.correo,c.nombre,c.apellido,c.fech_nac,c.foto, id_evaluador_fk,e.cedula,e.correo,e.nombre,e.apellido,e.fech_nac,e.foto,id_especialidad, informacion,remision, fecha, peticion.estado from peticion inner join cliente c on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad) inner join evaluador e on (id_evaluador_fk=id_evaluador) where id_peticion='". $this -> idPeticion ."'";
        }

        public function negar(){
          return "update peticion set estado='-1' where id_peticion='". $this -> idPeticion ."'";
        }

        public function aceptar(){
          return "update peticion set estado='1' where id_peticion='". $this -> idPeticion ."'";
        }

        public function corregir(){
          return "update peticion set estado='2' where id_peticion='". $this -> idPeticion ."'";
        }

        public function enObser(){
          return "update peticion set estado='-2' where id_peticion='". $this -> idPeticion ."'";
        }

        public function concretar(){
          return "update peticion set estado='3' where id_peticion='". $this -> idPeticion ."'";
        }

        public function consultarPeticionesPorCoC(){
          return "select id_peticion, id_evaluador_fk, nombre_especialidad, fecha,cedula, nombre, peticion.estado from peticion inner join cliente on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad) where id_cliente_fk='". $this -> id_cliente_fk ."' and peticion.estado='-2' and cliente.estado='1'";
        }

        public function consultarPeticionesAceptadas(){
          return "select id_peticion, id_cliente_fk, nombre_especialidad, fecha,cedula, nombre, id_evaluador_fk
                  from peticion inner join cliente on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad)
                  where id_cliente_fk='". $this -> id_cliente_fk ."' and peticion.estado='1' and cliente.estado='1'";
        }

        public function consultarPeticionesRechazadasCli(){
          return "select id_peticion, id_cliente_fk, nombre_especialidad, fecha,cedula, nombre, id_evaluador_fk
                  from peticion inner join cliente on (id_cliente_fk=id_cliente) inner join especialidad on (id_especialidad=especialidad)
                  where id_cliente_fk='". $this -> id_cliente_fk ."' and peticion.estado='-1' and cliente.estado='1'";
        }

        public function consultarPeticionesRechazadasEva(){
          return "select id_peticion, id_cliente_fk, nombre_especialidad, fecha,cedula, nombre, id_evaluador_fk
                  from peticion inner join evaluador on (id_evaluador_fk=id_evaluador) inner join especialidad on (id_especialidad=especialidad)
                  where id_evaluador_fk='". $this -> id_evaluador_fk ."' and peticion.estado='-1' and evaluador.estado='1'";
        }

        public function consultarCantidadPeticionesPorEstado(){
          return "select count(id_peticion) from peticion where estado='". $this -> estado."'";
        }
  }

?>
