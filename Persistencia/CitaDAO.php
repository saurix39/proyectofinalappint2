<?php

  class CitaDAO{

    private $idCita;
    private $id_cliente_fk;
    private $id_doctor_fk;
    private $id_peticion_fk;
    private $id_horario_fk;
    private $fecha;
    private $informacion;
    private $estado;

        public function CitaDAO($idCita="", $id_cliente_fk="", $id_doctor_fk="",$id_peticion_fk="",$id_horario_fk="",$fecha="",$informacion="",$estado=""){
              $this -> idCita = $idCita;
              $this -> id_cliente_fk = $id_cliente_fk;
              $this -> id_doctor_fk = $id_doctor_fk;
              $this -> id_peticion_fk = $id_peticion_fk;
              $this -> id_horario_fk = $id_horario_fk;
              $this -> fecha = $fecha;
              $this -> informacion = $informacion;
              $this -> estado = $estado;
        }

        public function listaCitasOcu($fechaI,$fechaF){
          return "select id_cita, id_cliente_fk, id_doctor_fk, id_peticion_fk, id_horario_fk, fecha, informacion, estado from cita where fecha between '". $fechaI ."' and '". $fechaF ."' and id_doctor_fk='". $this -> id_doctor_fk ."'";
        }

        public function insertar(){
          return "insert into cita (id_cliente_fk,id_doctor_fk,id_peticion_fk,id_horario_fk,fecha,informacion,estado)
                  values ('". $this -> id_cliente_fk ."','". $this -> id_doctor_fk ."','". $this -> id_peticion_fk ."','". $this -> id_horario_fk ."','". $this -> fecha ."','". $this -> informacion ."','". $this -> estado ."')";
        }

        public function listaCitasDeCliente(){
          return "select id_cita, id_cliente_fk, id_doctor_fk, id_peticion_fk, id_horario_fk, fecha, informacion, cita.estado
                  from cita inner join cliente on (id_cliente_fk=id_cliente)
                  where id_cliente_fk = '".$this -> id_cliente_fk."' order by fecha";
        }

        public function listaCitas(){
          return "select id_cita, id_cliente_fk, id_doctor_fk, id_peticion_fk, id_horario_fk, fecha, informacion, cita.estado
                  from cita inner join cliente on (id_cliente_fk=id_cliente) order by fecha";
        }


        public function listaCitasDeDoctor($cantidad,$pagina){
          return "select id_cita, id_cliente_fk, id_doctor_fk, id_peticion_fk, id_horario_fk, fecha, informacion, cita.estado
                  from cita inner join doctor on (id_doctor_fk=id_doctor)
                  where id_doctor_fk = '".$this -> id_doctor_fk."' order by fecha limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidadCitasDoc(){
          return "select count(id_cita) from cita inner join doctor on (id_doctor_fk=id_doctor) where id_doctor_fk = '".$this -> id_doctor_fk."'";
        }

        public function actualizar_Estado_Cit(){
            return "update cita
                    set estado = '" . $this -> estado. "'
                    where id_cita = '" . $this -> idCita .  "'";
        }

        public function consultar(){
          return "select id_cita, id_cliente_fk, id_doctor_fk, id_peticion_fk, id_horario_fk, fecha, informacion, estado
                  from cita
                  where id_cita = '".$this -> idCita."'";
        }

        public function consultarFechasDeCita(){
          return "select distinct(fecha)
                  from cita order by fecha";
        }

        public function consultarCantidadDeCitaActPorFecha(){
          return "select count(id_cita)
                  from cita
                  where fecha='".$this -> fecha."' and estado = '1'";
        }

        public function consultarCantidadDeCitaRecPorFecha(){
          return "select count(id_cita)
                  from cita
                  where fecha='".$this -> fecha."' and estado = '0'";
        }
  }

?>
