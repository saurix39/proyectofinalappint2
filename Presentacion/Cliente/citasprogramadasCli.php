<?php
$cita=new Cita("",$_SESSION["id"]);
$citas=$cita -> listaCitasDeCliente();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Listado de citas programadas</h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                <th scope="col">Id de cita</th>
                                <th scope="col">Nombre del doctor</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Horario</th>
                                <th scope="col">Proceso</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($citas as $cit){
                                        echo "<tr>";
                                        echo "<td>". $cit -> getIdCita() ."</td>";
                                        $doctor = new Doctor($cit -> getId_doctor_fk());
                                        $doctor -> consultar();
                                        echo "<td>". $doctor -> getNombre()." ".$doctor -> getApellido()."</td>";
                                        echo "<td>". $cit -> getFecha()."</td>";
                                        $horario = new Horario($cit -> getId_horario_fk());
                                        $horario -> consultar();
                                        echo "<td>"."Hora inicio: ".$horario -> getHora_inicio()."<br>"."Hora final: ".$horario -> getHora_final()."</td>";
                                        //echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Evaluador/gestionarPeticion.php") ."&peticion=". $pet -> getIdPeticion() ."'><i class='fas fa-keyboard' data-toggle='tooltip' data-placement='left' title='Concretar peticion'></i></a></td>";
                                        echo "<td><a href='reportePDF.php?cita=". $cit -> getIdCita() ."' target='_blank'><i class='fas fa-file-pdf' data-toggle='tooltip' data-placement='right' title='Documento PDF'></i></a></td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
