<?php
$idDoctor="";
if(isset($_GET["doctor"])){
$idDoctor=$_GET["doctor"];
}
$doctor=new Doctor($idDoctor);
$doctor -> consultarAjax();
date_default_timezone_set("America/Bogota");
$fecha=date('Y-m-j');
$fechas=array();
for($i=0;$i<180;$i++){
    array_push($fechas, date('Y-m-j',strtotime($fecha."+ ".$i." days")));
}
$cita = new Cita("","",$doctor -> getIdDoctor());
$citas = $cita -> listaCitasOcu($fecha,end($fechas));
$dispo= array();
$horas=array("6:00 AM-6:30 AM","6:30 AM-7:00 AM","7:00 AM-7:30 AM","7:30 AM-8:00 AM","8:00 AM-8:30 AM","8:30 AM-9:00 AM","9:00 AM-9:30 AM","9:30 AM-10:00 AM","2:00 PM-2:30 PM","2:30 PM-3:00 PM");
foreach($fechas as $fech){
    $horario=array();
    foreach($citas as $cit){
        if($cit -> getFecha()==$fech){
            array_push($horario,$cit -> getId_horario_fk());
        }
    }
    if($doctor -> getId_turno_fk()=="1"){
        for($i=1;$i<9;$i++){
            if(in_array($i,$horario)){
            }else{
                array_push($dispo,$fech."-".$i);
            }
        }
    }else{
        for($i=9;$i<11;$i++){
            if(in_array($i,$horario)){
            }else{
                array_push($dispo,$fech."-".$i);
            }
        }
    }
}
?>
<div class="input-group-prepend">
    <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
</div>
<select class="custom-select" name="turno" required>
    <option selected>Escoger...</option>
    <?php
    foreach($dispo as $fechdis){
        $horafech=explode("-",$fechdis);
        echo "<option value='". $fechdis ."'>".  $horafech[0] . "-" . $horafech[1] . "-" . $horafech[2] . " [" . $horas[($horafech[3]-1)] . "]" ."</option>";
    }
    ?>
</select>