<?php
$peticion=new Peticion("","",$_SESSION["id"]);
$peticiones=$peticion -> consultarPeticionesRechazadasCli();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Listado de peticiones negadas</h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                <th scope="col">Id de petición</th>
                                <th scope="col">Nombre del cliente</th>
                                <th scope="col">Cedula del cliente</th>
                                <th scope="col">Especialidad</th>
                                <th scope="col">Fecha de petición</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($peticiones as $pet){
                                        echo "<tr>";
                                        echo "<td>". $pet -> getIdPeticion() ."</td>";
                                        echo "<td>". $pet -> getId_cliente_fk() -> getNombre() ."</td>";
                                        echo "<td>". $pet -> getId_cliente_fk() -> getCedula() ."</td>";
                                        echo "<td>". $pet -> getEspecialidad() ."</td>";
                                        echo "<td>". $pet -> getFecha() ."</td>";
                                        echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Evaluador/gestionarPeticion.php") ."&peticion=". $pet -> getIdPeticion() ."'><i class='fas fa-eye' data-toggle='tooltip' data-placement='left' title='Ver informacion'></i></a></td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
