<?php
$registrado=false;
$error=0;
$doctor= new Doctor();
$nombres =  $doctor -> obtenerNombres();
if(isset($_POST["realizar"])){
    $idnombre=$doctor -> consultarIdNombre($_POST["especialidad"]);
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $descripcion=$_POST["descripcion"];
    $rutaLocal = $_FILES["imagen"]["tmp_name"];
    $tipo = $_FILES["imagen"]["type"];
    $tiempo = new DateTime();
    $rutaRemota = "Imagenes/Remision/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
    copy($rutaLocal,$rutaRemota);
    $peticion=new Peticion();
    $idEvaluador=$peticion -> asignarEvaluador();
    $peticion = new Peticion("",$idEvaluador,$_SESSION["id"],$idnombre,$descripcion,$rutaRemota,$fecha,"0");
    $peticion -> registrarPeticion();

    $doctor = new Doctor();
    $nombre_esp = $doctor -> consultarNombreId($idnombre);

    $informacion="Se registro un Peticion con la siguiente información:<br><strong>Identificacion del cliente:</strong> ".$_SESSION["id"]."<br><strong>Identificacion del evaluador:</strong> ".$idEvaluador."<br><strong>Especialidad:</strong> ".$nombre_esp."<br><strong>Fecha:</strong> ".$fecha."<br><strong>Informacion:</strong> ".$descripcion;
    $fecha = date('Y-m-j G-i-s');
    $accion = new Accion("","Solicitar_Peticion_Cliente");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
    $log -> insertarLogCliente();
    $registrado=true;
}
?>
<div class="container">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Petición de cita</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/peticion.php")?>" method="post" enctype="multipart/form-data">
                        <label>Especialidad</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="especialidad" required>
                                <option selected>Escoger...</option>
                                <?php
                                foreach($nombres as $nombreactual){
                                    echo "<option value='". $nombreactual ."'>". $nombreactual ."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Descripción</label>
                            <textarea class="form-control" name="descripcion" rows="5" required></textarea>
                        </div>
                        <label>Remisión</label>
                        <div class="custom-file mb-3">
                            <input type="file" name="imagen" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label" for="validatedCustomFile">Escoja la imagen...</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="realizar" class="form-control btn btn-secondary">Registrar peticion</button>
                        </div>
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Su peticion fue registrada, mantengase atento, pronto se le asignara una cita
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
