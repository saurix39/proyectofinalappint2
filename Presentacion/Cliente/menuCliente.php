<?php
$cliente= new Cliente($_SESSION["id"]);
$cliente -> consultar();
?>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/sesionCliente.php") ?>">Inicio</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Peticiones
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/peticion.php") ?>">Solicitar</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/enCorreccion.php") ?>">Por corregir</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/peticionesaceptadas.php") ?>">Peticiones aceptadas</a>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/peticionesnegadas.php") ?>">Peticiones negadas</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Citas
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/citasprogramadasCli.php") ?>">Citas programadas</a>
                </div>
            </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cliente: <?php echo $cliente -> getNombre()." ".$cliente -> getApellido()?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/actuInfoCliente.php") ?>">Editar perfil</a>
                    <a class="dropdown-item" href="#">Editar foto</a>
                    <a class="dropdown-item" href="#">Cambiar clave</a>
                </div>
                </li>
                <form class="form-inline" action="index.php?cerrarSesion=1" method="post">
                    <button class="btn btn-dark my-2 my-sm-0" type="submit">Cerrar sesión</button>
                </form>
             </ul>
        </div>    
    </nav>
</div>
