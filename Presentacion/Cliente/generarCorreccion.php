<?php
$registrado=false;
$error=0;
$peticion="";
if(isset($_GET["peticion"])){
$peticion=$_GET["peticion"];
}
if(isset($_POST["realizar"])){
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $peticionA=new Peticion($_GET["peticion"]);
    $descripcion=$_POST["descripcion"];
    $rutaLocal = $_FILES["imagen"]["tmp_name"];
    $tipo = $_FILES["imagen"]["type"];
    $tiempo = new DateTime();
    $rutaRemota = "Imagenes/Remision/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
    copy($rutaLocal,$rutaRemota);
    $edicionPeticion=new EdicionPeticion("",$peticion,$descripcion,$rutaRemota,$fecha);
    $edicionPeticion -> insertar();
    $peticionA -> corregir();

    $cliaux = new Cliente($_SESSION["id"]);
    $cliaux -> consultar();
    $informacion="Se registra la correcion de  la peticion con la siguiente información:<br><strong>Identificacion de cliente:</strong> ".$_SESSION["id"]."<br><strong>Cedula de cliente:</strong> ".$cliaux -> getCedula()."<br><strong>Descripcion:</strong> ".$_POST["descripcion"];
    $fecha = date('Y-m-j G-i-s');
    $accion = new Accion("","Generar_Correccion");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
    $log -> insertarLogCliente();

    $registrado=true;
}
?>
<div class="container">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Corrección</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/generarCorreccion.php")?>&peticion=<?php echo $peticion?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label >Descripción</label>
                            <textarea class="form-control" name="descripcion" rows="5" required></textarea>
                        </div>
                        <label>Remisión</label>
                        <div class="custom-file mb-3">
                            <input type="file" name="imagen" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label" for="validatedCustomFile">Escoja la imagen...</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="realizar" class="form-control btn btn-secondary">Registrar corrección</button>
                        </div>
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Su corrección fue registrada, mantengase atento, pronto se le asignara una cita
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
