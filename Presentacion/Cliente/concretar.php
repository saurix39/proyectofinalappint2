<?php
  $error=0;
  $registrado=false;
  $peticion="";
  if(isset($_GET["peticion"])){
    $peticion=new Peticion($_GET["peticion"]);
    $peticion -> consultarInfo();
    $doctor = new Doctor("","","","",$peticion -> getEspecialidad());
    $doctores = $doctor -> listaDoc();
      if(isset($_POST["concretarcita"])){
        $fechaObte=$_POST["turno"];
        $arrfechh=explode("-",$fechaObte);
        $fechaSola=$arrfechh[0]."-".$arrfechh[1]."-".$arrfechh[2];
        $horarioEs=$arrfechh[3];
        $citaFinal=new Cita("",$_SESSION["id"],$_POST["doctor"],$peticion -> getIdPeticion(),$horarioEs,$fechaSola,$_POST["descripcion"],"1");
        $citaFinal -> insertar();
        $peticion -> concretar();
        date_default_timezone_set("America/Bogota");
        $fecha=date('Y-m-j G-i-s');
        $acpet=new AcPeticionesCli("",$peticion -> getIdPeticion(),$_SESSION["id"],"3",$fecha);
        $acpet -> insertar();

        $cliaux = new Cliente($_SESSION["id"]);
        $cliaux -> consultar();
        $informacion="Se concreto la peticion para la activacion de la cita con la siguiente información:<br><strong>Identificacion de cliente:</strong> ".$_SESSION["id"]."<br><strong>Cedula de cliente:</strong> ".$cliaux -> getCedula()."<br><strong>Identificacion de doctor:</strong> ".$_POST["doctor"]."<br><strong>Descripcion:</strong> ".$_POST["descripcion"];
        $fecha = date('Y-m-j G-i-s');
        $accion = new Accion("","Concretar");
        $accion -> consultarAcciones();

        $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
        $log -> insertarLogCliente();


?>
  <meta http-equiv="refresh" content="0; url=index.php?pid=<?php echo base64_encode("Presentacion/Cliente/sesionCliente.php")?>&ac=concre">
<?php
      }
  }
?>
<div class="container">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-md-8">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Concretar cita</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/concretar.php")?>&peticion=<?php echo $peticion -> getIdPeticion()?>" method="post" enctype="multipart/form-data">
                        <label>Doctor</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="doctor" id="doctor" required>
                                <option selected>Escoger...</option>
                                <?php
                                foreach($doctores as $doc){
                                    echo "<option value='". $doc -> getIdDoctor() ."'>". $doc -> getNombre()." ". $doc -> getApellido() ."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <label>Fecha y horario</label>
                        <div class="input-group mb-3" id="horarioD">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="turno" required>
                                <option selected>Escoger...</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Descripción</label>
                            <textarea class="form-control" name="descripcion" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="concretarcita" class="form-control btn btn-secondary">Concretar cita</button>
                        </div>
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php }else if($error==1){ ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                Este correo ya fue registrado!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$("#doctor").on("change", function() {
	url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Cliente/horarioAjax.php") ?>&doctor=" + $(this).val();
	$("#horarioD").load(url);
});
</script>
