<div class="container mt-1">
    <div class="row">
        <div class="col">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="Img/quiro1.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="Img/editar.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="Img/editar2.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-md-4 col-12 mt-1">
            <div class="card-boddy">
                <img src="Img/medicina.jpg" class="rounded" alt="" width="100%">
                <a href="#" class="btn btn-primary btn-block mt-3" role="button" aria-pressed="true">Informacion general</a>
            </div>
        </div>
        <div class="col-md-4 col-12 mt-1">
            <div class="card-boddy">
                <img src="Img/medicina2.jpg" class="rounded" alt="" width="100%">
                <a href="#" class="btn btn-primary btn-block mt-3" role="button" aria-pressed="true" data-toggle="modal" data-target="#Autenticar">Inicia sesión</a>
            </div>
        </div>
        <div class="col-md-4 col-12 mt-1">
            <div class="card-boddy">
                <img src="Img/medicina3.jpg" class="rounded" alt="" width="100%">
                <a href="#" class="btn btn-primary btn-block mt-3" role="button" aria-pressed="true">Poner queja</a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Autenticar" tabindex="-1" role="dialog" aria-labelledby="AutenticarLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="AutenticarLabel">Formulario de autenticación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="index.php?pid=<?php echo base64_encode("Presentacion/Autenticar.php")?>" method="post">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Correo:</label>
                    <input type="email" name="correo" class="form-control" id="recipient-name" required>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Clave:</label>
                    <input type="password" name="clave" class="form-control" id="recipient-name" required>
                </div>
                <div class="form-group">
                    <button type="submit" name="ingresar" class="form-control btn btn-primary">Ingresar</button>
                </div>
                <?php
                if($error==3){
                    echo "<div class='alert alert-danger' role='alert'>";
                    echo "Su usuario se encuentra deshabilitado";
                    echo "</div>";
                }else if($error==1){
                    echo "<div class='alert alert-danger' role='alert'>";
                    echo "Los datos ingresados no concuerdan, intente de nuevo";
                    echo "</div>";
                }
                ?>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
