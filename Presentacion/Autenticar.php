<?php
$correo=$_POST["correo"];
$clave=$_POST["clave"];
$administrador=new Administrador("","",$correo,$clave);
$cliente=new Cliente("","",$correo,$clave);
$doctor=new Doctor("","",$correo,$clave);
$evaluador=new Evaluador("","",$correo,$clave);
date_default_timezone_set("America/Bogota");
if($administrador -> autenticar()){
    if($administrador -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$administrador -> getIdAdministrador();
        $_SESSION["rol"]="Administrador";
        $adminaux = new Administrador($_SESSION["id"]);
        $adminaux -> consultar();
        $informacion="Se registro un inicio de sesion Administrador con la siguiente información:<br><strong>Cedula:</strong> ".$adminaux -> getCedula()."<br><strong>Correo:</strong> ".$correo."<br><strong>Nombre:</strong> ".$adminaux -> getNombre();
        $fecha = date('Y-m-j G-i-s');
        $accion = new Accion("","Inicio_sesion_Administrador");
        $accion -> consultarAcciones();

        $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
        $log -> insertarLogAdministrador();
        header("Location: index.php?pid=".base64_encode("Presentacion/Administrador/sesionAdministrador.php"));
    }
}else if($cliente -> autenticar()){
    if($cliente -> getEstado()==-1){
        header("Location: index.php?error=2");
    }else if($cliente -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$cliente -> getIdCliente();
        $_SESSION["rol"]="Cliente";
        $clienteaux = new Cliente($_SESSION["id"]);
        $clienteaux -> consultar();
        $informacion="Se registro un inicio de sesion Cliente con la siguiente información:<br><strong>Cedula:</strong> ".$clienteaux -> getCedula()."<br><strong>Correo:</strong> ".$correo."<br><strong>Nombre:</strong> ".$clienteaux -> getNombre();
        $fecha = date('Y-m-j G-i-s');
        $accion = new Accion("","Inicio_sesion_Cliente");
        $accion -> consultarAcciones();

        $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
        $log -> insertarLogCliente();
        header("Location: index.php?pid=".base64_encode("Presentacion/Cliente/sesionCliente.php"));
    }
}else if($doctor -> autenticar()){
    if($doctor -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$doctor -> getIdDoctor();
        $_SESSION["rol"]="Doctor";
        $doctoraux = new Doctor($_SESSION["id"]);
        $doctoraux -> consultar();
        $informacion="Se registro un inicio de sesion Doctor con la siguiente información:<br><strong>Cedula:</strong> ".$doctoraux -> getCedula()."<br><strong>Correo:</strong> ".$correo."<br><strong>Nombre:</strong> ".$doctoraux -> getNombre();
        $fecha = date('Y-m-j G-i-s');
        $accion = new Accion("","Inicio_sesion_Doctor");
        $accion -> consultarAcciones();

        $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
        $log -> insertarLogDoctor();
        header("Location: index.php?pid=".base64_encode("Presentacion/Doctor/sesionDoctor.php"));
    }
}else if($evaluador -> autenticar()){
    if($evaluador -> getEstado()==0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"]=$evaluador -> getIdEvaluador();
        $_SESSION["rol"]="Evaluador";
        $evaluadoraux = new Evaluador($_SESSION["id"]);
        $evaluadoraux -> consultar();
        $informacion="Se registro un inicio de sesion Evaluador con la siguiente información:<br><strong>Cedula:</strong> ".$evaluadoraux -> getCedula()."<br><strong>Correo:</strong> ".$correo."<br><strong>Nombre:</strong> ".$evaluadoraux -> getNombre();
        $fecha = date('Y-m-j G-i-s');
        $accion = new Accion("","Inicio_sesion_Evaluador");
        $accion -> consultarAcciones();

        $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
        $log -> insertarLogEvaluador();
        header("Location: index.php?pid=".base64_encode("Presentacion/Evaluador/sesionEvaluador.php"));
    }
}else{
    header("Location: index.php?pid='". base64_encode("Presentacion/formAutenticar.php") ."'&error=1");
}
?>
