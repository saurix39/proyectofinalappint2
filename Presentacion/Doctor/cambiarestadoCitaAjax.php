<?php
  $citOld = new Cita($_POST["idCit"]);
  $citOld -> consultar();
  $informacion="Se registro un cambio de estado de la cita con la siguiente información:<br><strong>N° Cita:</strong> ".$citOld -> getIdCita()."<br><strong>Cliente:</strong> ".$citOld -> getId_cliente_fk()."<br><strong>Doctor:</strong> ".$citOld -> getId_doctor_fk()."<br><strong>Estado Anterior:</strong> ".$citOld -> getEstado();
  $fecha = date('Y-m-j G-i-s');
  $accion = new Accion("","Actualizar_Estado_Cita");
  $accion -> consultarAcciones();

  $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
  $log -> insertarLogDoctor();

  $cit = new Cita($_POST["idCit"],"","","","","","",$_POST["estado"]);
  $cit -> actualizar_Estado_Cit();

?>
