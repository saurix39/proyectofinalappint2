<?php
$doctor= new Doctor($_SESSION["id"]);
$doctor -> consultar();
?>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/Doctor/sesionDoctor.php") ?>">Inicio</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/Doctor/citasprogramadasDoc.php") ?>">Citas programadas</a>
            </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Doctor: <?php echo $doctor -> getNombre()." ".$doctor -> getApellido()?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Doctor/actuInfoDoctor.php") ?>">Editar perfil</a>
                    <a class="dropdown-item" href="#">Editar foto</a>
                    <a class="dropdown-item" href="#">Cambiar clave</a>
                </div>
                </li>
                <form class="form-inline" action="index.php?cerrarSesion=1" method="post">
                    <button class="btn btn-dark my-2 my-sm-0" type="submit">Cerrar sesión</button>
                </form>
            </ul>
        </div>
    </nav>
</div>
