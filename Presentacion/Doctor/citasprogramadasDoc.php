<?php
  $cita=new Cita("","",$_SESSION["id"]);
  $cantidad = 5;
  if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
  }
  $arrayCit = $cita -> listaCitasDeDoctor($cantidad, $pagina);
  $totalRegistros = $cita -> consultarCantidadCitasDoc();
  $totalPaginas = intval($totalRegistros/$cantidad);
  if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
  }
  $ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Listado de citas programadas</h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table">
                          <thead class="thead-dark">
                              <tr>
                              <th scope="col">Id de cita</th>
                              <th scope="col">Nombre del cliente</th>
                              <th scope="col">Fecha</th>
                              <th scope="col">Horario</th>
                              <th scope="col">informacion</th>
                              <th scope="col">Estado</th>
                              <th scope="col"></th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php
                                  foreach($arrayCit as $cit){
                                      echo "<tr>";
                                      echo "<td>". $cit -> getIdCita() ."</td>";
                                      $cli = new Cliente($cit -> getId_cliente_fk());
                                      $cli -> consultar();
                                      echo "<td>". $cli -> getNombre()." ".$cli -> getApellido()."</td>";
                                      echo "<td>". $cit -> getFecha()."</td>";
                                      $horario = new Horario($cit -> getId_horario_fk());
                                      $horario -> consultar();
                                      echo "<td>"."Hora inicio: ".$horario -> getHora_inicio()."<br>"."Hora final: ".$horario -> getHora_final()."</td>";
                                      echo "<td>". $cit -> getInformacion()."</td>";
                              ?>
                                    <td>
                                      <select class="custom-select camestado_cita" data-idcita="<?php echo $cit -> getIdCita()?>" style="width: 100px;" >
                                        <option value="1" <?php echo ($cit->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
                                        <option value="0" <?php echo ($cit->getEstado() == 0) ? 'selected' : '' ?>>Cancelada</option>
                                      </select>
                                    </td>
                              <?php
                                      echo "<td><a href='index.php?pid=". base64_encode("Presentacion/Evaluador/gestionarPeticion.php") ."&peticion=". $cit -> getId_peticion_fk() ."'><i class='fas fa-keyboard' data-toggle='tooltip' data-placement='left' title='Ver informacion'></i></a></td>";
                                      echo "</tr>";
                                  }
                              ?>
                          </tbody>
                      </table>
                    </div>
                    <div class="d-flex justify-content-end">
                        <nav>
                          <ul class="pagination">
                            <li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/Doctor/citasprogramadasDoc.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                            <?php
                            for($i=1; $i<=$totalPaginas; $i++){
                                if($i==$pagina){
                                    echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                                }else{
                                    echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Doctor/citasprogramadasDoc.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                                }
                            }
                            ?>
                            <li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("Presentacion/Doctor/citasprogramadasDoc.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                          </ul>
                        </nav>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $(".camestado_cita").change(function() {
      var objJSON = {
          idCit: $(this).data("idcita"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Doctor/cambiarestadoCitaAjax.php") ?>";

      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });
</script>
