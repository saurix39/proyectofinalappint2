<?php
$doctor= new Doctor($_SESSION["id"]);
$doctor -> consultar();
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">  
                <div class="d-flex justify-content-center">
                        <img src="<?php echo ($doctor-> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$doctor -> getFoto() ?>" alt="foto del usuario" width="30%">
                </div>
                <table class="table mt-1">
                    <tbody>
                        <tr>
                        <th scope="row">Nombre</th>
                        <td><?php echo $doctor ->getNombre() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Apellido</th>
                        <td><?php echo $doctor ->getApellido() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Cedula</th>
                        <td><?php echo $doctor ->getCedula() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Correo</th>
                        <td><?php echo $doctor ->getCorreo() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Especialidad</th>
                        <td><?php echo $doctor ->getEspecialidad() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Fecha de nacimiento</th>
                        <td><?php echo $doctor ->getFech_nac() ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>