<?php
$error=0;
$registrado=false;
$doctor= new Doctor();
$nombres =  $doctor -> obtenerNombres();
if(isset($_POST["registrar"])){
    $idnombre=$doctor -> consultarIdNombre($_POST["especialidad"]);
    date_default_timezone_set("America/Bogota");
    $cedula=$_POST["cedula"];
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $correo=$_POST["correo"];
    $clave=$_POST["clave"];
    $fech_nac=$_POST["fech_nac"];
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Administrador/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $doctor = new Doctor("",$cedula,$correo,$clave,$idnombre,$nombre,$apellido,$fech_nac,"1",$rutaRemota, $_POST["turno"]);
        $turno = new Turno($_POST["turno"]);
        $turno -> consultarNombreTurno();
        if($doctor -> existeCorreo()){
            $error=1;
        }else{

            $doctor -> registrarDoctor();
            $nombre_esp = $doctor -> consultarNombreId($idnombre);
            $informacion="Se registro un doctor con la siguiente información:<br><strong>Cedula:</strong> ".$cedula."<br><strong>Nombre:</strong> ". $nombre ."<br><strong>Apellido:</strong> ". $apellido ."<br><strong>Correo:</strong> ". $correo ."<br><strong>Especialidad:</strong> ".$nombre_esp."<br><strong>Turno:</strong> ".$turno -> getNombre()."<br><strong>Fecha de nacimiento:</strong> ". $fech_nac;
            $fecha = date('Y-m-j G-i-s');
            $accion = new Accion("","Crear_Doctor");
            $accion -> consultarAcciones();

            $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
            $log -> insertarLogAdministrador();
            $registrado=true;
        }
    }else{
        $turno = new Turno($_POST["turno"]);
        $turno -> consultarNombreTurno();
        $doctor = new Doctor("",$cedula,$correo,$clave,$idnombre,$nombre,$apellido,$fech_nac,"1","", $_POST["turno"]);
        if($doctor -> existeCorreo()){
            $error=1;
        }else{
            $doctor -> registrarDoctor();
            $nombre_esp = $doctor -> consultarNombreId($idnombre);
            $informacion="Se registro un doctor con la siguiente información:<br><strong>Cedula:</strong> ".$cedula."<br><strong>Nombre:</strong> ". $nombre ."<br><strong>Apellido:</strong> ". $apellido ."<br><strong>Correo:</strong> ". $correo ."<br><strong>Especialidad:</strong> ".$nombre_esp."<br><strong>Turno:</strong> ".$turno -> getNombre()."<br><strong>Fecha de nacimiento:</strong> ". $fech_nac;
            $fecha = date('Y-m-j G-i-s');
            $accion = new Accion("","Crear_Doctor");
            $accion -> consultarAcciones();

            $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
            $log -> insertarLogAdministrador();
            $registrado=true;
        }
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Registro de doctor</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Doctor/registrarDoctor.php")?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input type="text" name="cedula" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento</label>
                            <input type="date" name="fech_nac" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="email" name="correo" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Clave</label>
                            <input type="password" name="clave" class="form-control" required>
                        </div>
                        <label>Especialidad</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="especialidad" required>
                                <option selected>Escoger...</option>
                                <?php
                                foreach($nombres as $nombreactual){
                                    echo "<option value='". $nombreactual ."'>". $nombreactual ."</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <label>Turno</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Opciones</label>
                            </div>
                            <select class="custom-select" name="turno" required>
                                <option selected>Escoger...</option>
                                <option value='1'>Mañana</option>
                                <option value='2'>Tarde</option>
                            </select>
                        </div>
                        <label>Imagen</label>
                        <div class="custom-file mb-3">
                            <input type="file" name="imagen" class="custom-file-input" id="validatedCustomFile">
                            <label class="custom-file-label" for="validatedCustomFile">Escoja la imagen...</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar doctor</button>
                        </div>
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php }else if($error==1){ ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                Este correo ya fue registrado!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
