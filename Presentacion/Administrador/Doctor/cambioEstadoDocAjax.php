<?php
  $docOld = new Doctor($_POST["idDoc"]);
  $docOld -> consultar();

  date_default_timezone_set('America/Bogota');
  $informacion="Se registro un cambio de estado al doctor con la siguiente información:<br><strong>Cedula:</strong> ".$docOld -> getCedula()."<br><strong>Nombre:</strong> ". $docOld -> getNombre() ."<br><strong>Correo:</strong>: ". $docOld -> getCorreo() ."<br><strong>Especialidad:</strong> ". $docOld -> getEspecialidad() ."<br><strong>Estado Anterior:</strong> ". $docOld -> getEstado();
  $fecha = date('Y-m-j G-i-s');
  $accion = new Accion("","Actualizar_Estado_Doctor");
  $accion -> consultarAcciones();

  $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
  $log -> insertarLogAdministrador();

  $doc = new Doctor($_POST["idDoc"],"","","","","","","",$_POST["estado"]);
  $doc -> actualizar_Estado_Doc();
?>
