<?php
  $doc = new Doctor($_GET["iddoc"]);
  $doc -> consultar();
?>
<div class="container-fluid">
  <div class="d-flex justify-content-center">
          <img src="<?php echo ($doc -> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$doc -> getFoto() ?>" alt="foto del usuario" width="200px">
  </div>
  <table class="table mt-1">
      <tbody>
          <tr>
          <th scope="row">Especialidad</th>
          <td><?php echo $doc ->getEspecialidad() ?></td>
          </tr>
          <tr>
          <th scope="row">Nombre</th>
          <td><?php echo $doc ->getNombre() ?></td>
          </tr>
          <tr>
          <th scope="row">Apellido</th>
          <td><?php echo $doc ->getApellido() ?></td>
          </tr>
          <tr>
          <th scope="row">Cedula</th>
          <td><?php echo $doc ->getCedula() ?></td>
          </tr>
          <tr>
          <th scope="row">Correo</th>
          <td><?php echo $doc ->getCorreo() ?></td>
          </tr>
          <tr>
          <th scope="row">Fecha de nacimiento</th>
          <td><?php echo $doc ->getFech_nac() ?></td>
          </tr>
      </tbody>
  </table>
</div>
