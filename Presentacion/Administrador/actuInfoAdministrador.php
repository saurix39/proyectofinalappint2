<?php
$actualizado=false;
if(isset($_POST["actualizar"])){
    date_default_timezone_set("America/Bogota");
    $cedula=$_POST["cedula"];
    $nombre=$_POST["nombre"];
    $apellido=$_POST["apellido"];
    $fech_nac=$_POST["fech_nac"];
    $administrador =new Administrador($_SESSION["id"]);
    $administrador -> consultar();

    $informacion="Se registro una actualizacion de datos del Administrador con la siguiente información:<br><strong>Cedula:</strong> ".$administrador -> getCedula()."<br><strong>Correo:</strong> ". $administrador -> getCorreo() ."<br><strong>Nombre:</strong> ". $administrador -> getNombre() ."<br><strong>Apellido:</strong> ". $administrador -> getApellido() ."<br><strong>Fecha de nacimiento:</strong> ". $administrador -> getFech_nac();
    $fecha = date('Y-m-j G-i-s');
    $accion = new Accion("","Actualizar_Datos_Administrador");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
    $log -> insertarLogAdministrador();

    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "Imagenes/Administrador/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        if($administrador -> getFoto()!=""){
            unlink($administrador -> getFoto());
        }


        $administrador = new Administrador($_SESSION["id"],$cedula,"","",$nombre,$apellido,$fech_nac,"1",$rutaRemota);
        $administrador -> actualizarInformacion();

        $actualizado=true;
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Actualizar información</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/actuInfoAdministrador.php")?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Cedula</label>
                            <input type="text" name="cedula" class="form-control" value="<?php echo $administrador-> getCedula() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" value="<?php echo $administrador-> getNombre() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" name="apellido" class="form-control" value="<?php echo $administrador-> getApellido() ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento</label>
                            <input type="date" name="fech_nac" class="form-control" value="<?php echo $administrador-> getFech_nac() ?>" required>
                        </div>
                        <label>Imagen</label>
                        <div class="custom-file mb-3">
                            <input type="file" name="imagen" class="custom-file-input" id="validatedCustomFile" required>
                            <label class="custom-file-label" for="validatedCustomFile">Escoja la imagen...</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="actualizar" class="form-control btn btn-secondary">Actualizar información</button>
                        </div>
                        <?php if($actualizado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
