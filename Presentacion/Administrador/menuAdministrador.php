<?php
$administrador= new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>

<div class="container-fluid">
  <div class="row">
    <div class="col">
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/sesionAdministrador.php") ?>">Inicio</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Cliente
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Cliente/listaCliente.php")?>">Lista</a>
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Cliente/registrarCliente.php") ?>">Registrar</a>
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/listaCitas.php") ?>">Lista de citas</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Doctor
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Doctor/listaDoctor.php")?>">Lista</a>
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Doctor/registrarDoctor.php") ?>">Registrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Evaluador
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php")?>">Lista</a>
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Evaluador/registrarEvaluador.php") ?>">Registrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Especialidad
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Especialidad/listaespecialidad.php")?>">Lista</a>
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Especialidad/registrarEspecialidad.php")?>">Registrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Administrador
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Administradores/listaAdministradores.php")?>">Lista</a>
                            <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Administrador/registrarAdministrador.php") ?>">Registrar</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Log
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Log/logAdministrador.php")?>">Log Administrador</a>
                          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Log/logDoctor.php")?>">Log Doctor</a>
                          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Log/logCliente.php")?>">Log Cliente</a>
                          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Log/logEvaluador.php")?>">Log Evaluador</a>
                          </div>
                      </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/graficas.php")?>">Graficas</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Administrador: <?php echo $administrador -> getNombre()." ".$administrador -> getApellido()?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/actuInfoAdministrador.php") ?>">Editar perfil</a>
                                <a class="dropdown-item" href="#">Editar foto</a>
                                <a class="dropdown-item" href="#">Cambiar clave</a>
                            </div>
                        </li>
                        <form class="form-inline" action="index.php?cerrarSesion=1" method="post">
                            <button class="btn btn-dark my-2 my-sm-0" type="submit">Cerrar sesión</button>
                        </form>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
