<?php
  /// Grafica 1.
  $esp1= new Especialidad();
  $listaesp1= $esp1 -> consultar();

  $arrayCantidadDoc1 = array();
  foreach ($listaesp1 as $lis1) {
    $doc1 = new Doctor("","","","", $lis1 -> getIdEspecialidad());
    $cantesp1 = $doc1 -> consultarCantidadEspecialidad();
    array_push($arrayCantidadDoc1,$cantesp1);
  }

  //////////////
  //// Grafica 2.
  $arrayEstaPet2 = array('-2','-1','0','1','2','3');
  $arrayNombEstaPet2 = array('Por corregir','Negada','Pendiente','Aprobada','Corregida','Concretada');
  $arrayCantidadPet2 = array();
  foreach ($arrayEstaPet2 as $list2) {
    $peticion2 = new Peticion("","","","","","","",$list2);
    $cantPet2 = $peticion2 -> consultarCantidadPeticionesPorEstado();
    array_push($arrayCantidadPet2,$cantPet2);
  }

  ///////////////////
  /////Grafica 3
  $cit3 = new Cita();
  $listfechCit3 = $cit3 -> consultarFechasDeCita();
  $arrayCantidaCitaAct3 = array();
  $arrayCantidaCitaRec3 = array();

  foreach ($listfechCit3 as $list3) {
    $citact= new Cita("","","","","",$list3 -> getFecha());
    $cantcitact = $citact -> consultarCantidadDeCitaActPorFecha();
    array_push($arrayCantidaCitaAct3,$cantcitact);

    $citrec= new Cita("","","","","",$list3 -> getFecha());
    $cantcitrec = $citrec -> consultarCantidadDeCitaRecPorFecha();
    array_push($arrayCantidaCitaRec3,$cantcitrec);
  }
  $fechaseparada = array();
  foreach ($listfechCit3 as $list3) {
    $fechasepar = explode('-',$list3 -> getFecha());
    array_push($fechaseparada,$fechasepar);
  }

  /////////////////////////
  /////Grafica 4

  $cliente4 = new Cliente();
  $estadoactcli4= $cliente4 -> consultarCantidadEstadoActivo();
  $estadoincli4= $cliente4 -> consultarCantidadEstadoInactivo();

  /////////////////
  ////////Grafica 5

  $doctor5 = new Doctor();
  $doctores5= $doctor5 -> consultarDoctores();
  $arrayCantidadCitas5 = array();
  foreach ($doctores5 as $lisdoc5) {
    $cit5 = new Cita("","",$lisdoc5 -> getIdDoctor());
    $cantcitas5= $cit5 -> consultarCantidadCitasDoc();
    array_push($arrayCantidadCitas5,$cantcitas5);
  }

?>
<div class="container mt-3">
  <div class="row">
      <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-header">
                <h3>Cantidad de doctores por especialidad</h3>
            </div>
            <div class="card-body">
              <div class="row d-flex justify-content-center">
                <div class="col-8">
                  <div id="grafica1" style="width: 400px; height: 300px;"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-header">
                <h3>Cantidad de peticiones por estado</h3>
            </div>
            <div class="card-body">
              <div class="row d-flex justify-content-center">
                <div class="col-8">
                  <div id="grafica2" style="width: 500px; height: 300px;"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  <div class="row">
      <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-header">
                <h3>Cantidad de citas por dia</h3>
            </div>
            <div class="card-body">
              <div class="row d-flex justify-content-center">
                <div class="col-8">
                  <div id="grafica3" style="width: 400px; height: 300px;"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="col-md-6 col-12">
        <div class="card">
            <div class="card-header">
                <h3>Cantidad de clientes por estado</h3>
            </div>
            <div class="card-body">
              <div class="row d-flex justify-content-center">
                <div class="col-8">
                  <div id="grafica4" style="width: 500px; height: 300px;"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  <div class="row d-flex justify-content-center">
    <div class="card">
        <div class="card-header">
            <h3>Cantidad de citas por doctor</h3>
        </div>
        <div class="card-body">
          <div id="grafica5" style="width: 900px; height: 500px;"></div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Especialidad', 'Cantidad de doctores'],
          <?php
          for ($i=0; $i < count($listaesp1) ; $i++) {
              echo "['".$listaesp1[$i] -> getNombre()."',".implode(", ",$arrayCantidadDoc1[$i])."], ";
          }
          ?>
        ]);

        var options = {
          title: 'Cantidad de doctores por especialidad',
          bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById('grafica1'));
        chart.draw(data, options);
      };
</script>


<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Estado', 'Cantidad de peticiones'],
          <?php
          for ($i=0; $i < count($arrayEstaPet2) ; $i++) {
              echo "['".$arrayNombEstaPet2[$i]."',".implode(", ",$arrayCantidadPet2[$i])."], ";
          }
          ?>
        ]);

        var options = {
          title: 'Cantidad de peticiones por estado',
          is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('grafica2'));

        chart.draw(data, options);
      }
</script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Fecha', 'Citas Activas', 'Citas Rechazadas'],
          <?php
          for ($i=0; $i < count($arrayCantidaCitaRec3) ; $i++) {
              echo "['".$listfechCit3[$i] -> getFecha()."',".$arrayCantidaCitaAct3[$i].",".$arrayCantidaCitaRec3[$i]."],";
          }
          ?>
        ]);

        var options = {
          title: 'Cantidad de citas por dia',
          bars: 'horizontal'
        };

      var chart = new google.charts.Bar(document.getElementById('grafica3'));
      chart.draw(data, options);
    };
</script>


<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Estado', 'Cantidad de clientes'],
          ['Activos',<?php echo $estadoactcli4[0] ?>],
          ['Inactivos',<?php echo $estadoincli4[0] ?>],
        ]);

        var options = {
          title: '',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('grafica4'));
        chart.draw(data, options);
      }
</script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Doctor', 'Cantidad de citas'],
          <?php
          for ($i=0; $i < count($doctores5) ; $i++) {
              echo "['".$doctores5[$i] -> getIdDoctor()."',".$arrayCantidadCitas5[$i]."],";
          }
          ?>
        ]);

        var options = {
          title: 'Cantidad de doctores por especialidad'
        };

        var chart = new google.charts.Bar(document.getElementById('grafica5'));
        chart.draw(data, options);
      };
</script>
