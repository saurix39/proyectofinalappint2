<?php
$log = new Log();
$cantidad = 15;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $log -> consultarCantidadLogAdm($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$LogAdm = $log -> consultarFiltroPaginacionLogAdm($filtro, $cantidad, $pagina);
?>
<div id="contenido">
    <div class="table table-responsive-sm table-responsive-md">
      <table class="table table-responsive-md table-hover table-striped">
        <tr class="thead-dark">
          <th>#</th>
          <th>Accion</th>
          <th>Actor</th>
          <th>Fecha</th>
          <th>Informacion</th>
        </tr>
        <?php
        $i=1;
        foreach($LogAdm as $logsa){
            echo "<tr>";
            echo "<td>" . $i . "</td>";

            $accion = new Accion($logsa -> getIdAccion());
            $accion -> consultarNombresAcciones();
            echo "<td>" . $accion -> getNombre() . "</td>";
            echo "<td>" . $logsa -> getIdActor() . "</td>";
            echo "<td>" . $logsa -> getFecha() . "</td>";
            echo "<td><a id='modal". $logsa -> getIdLog()."' data-toggle='tooltip' data-placement='top' title='Informacion del log'><i class='fas fa-info-circle'></i></a></td>";
            echo "</tr>";
            $i++;
        }
        ?>
      </table>
    </div>
    <div class="d-flex justify-content-end">
        <nav>
            <ul class="pagination">
                <?php if ($pagina > 1) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Log/logAdministrador.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                } ?>
                <?php for ($i = 1; $i <= $cantPagina; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='page-item active'>" .
                            "<a class='page-link'>$i</a>" .
                            "</li>";
                    } else {
                        echo "<li class='page-item'>" .
                            "<a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Administrador/Log/logAdministrador.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                            "</li>";
                    }
                } ?>
                <?php if ($pagina < $cantPagina) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Log/logAdministrador.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                } ?>
            </ul>
        </nav>
    </div>
</div>

<script>
    $(document).ready(function(){
    <?php
    foreach($LogAdm as $logActual){
    ?>
        $("#modal<?php echo $logActual -> getIdLog()?>").click(function(e){
            var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Log/modalInformacionLogAdmAjax.php") ?>&idlog=<?php echo $logActual -> getIdLog() ?>";
            $("#modalInfo").load(url);
            $("#modalSup").modal();
        });
    <?php } ?>
  });
</script>
