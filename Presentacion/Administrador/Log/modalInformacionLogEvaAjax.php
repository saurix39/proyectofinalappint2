<?php
  $log = new Log($_GET["idlog"]);
  $log -> consultarLogEva();

  $accion = new Accion($log -> getIdAccion());
  $accion -> consultarNombresAcciones();

  $evalu = new Evaluador($log -> getIdActor());
  $evalu -> consultar();
?>
<div class="container-fluid">
    <?php
      if ($accion -> getNombre() == "Inicio_sesion_Evaluador" || $accion -> getNombre() == "Actualizar_Datos_Evaluador" ||
          $accion -> getNombre() == "Generar_Observacion" || $accion -> getNombre() == "Generar_Correccion" || $accion -> getNombre() == "Negar" ||
          $accion -> getNombre() == "Negar" || $accion -> getNombre() == "Generar_Edicion" || $accion -> getNombre() == "Aceptar") {
        ?>
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Informacion del actor</h3>
                    </div>
                    <div class="card-body d-flex justify-content-center">
                        <?php echo  "Cedula: ".$evalu -> getCedula()."<br>".
                                    "Nombre: ".$evalu -> getNombre()."<br>".
                                    "Apellido: ".$evalu -> getApellido()."<br>".
                                    "Correo: ".$evalu -> getCorreo()."<br>"?>
                        <img src="<?php echo $evalu -> getFoto()?>" width="150px">
                    </div>
                </div>
            </div>
            <div class="col-6">
              <div class="card">
                  <div class="card-header">
                      <h3>Datos de log</h3>
                  </div>
                  <div class="card-body">
                      <?php echo  $log -> getInformacion()?>
                  </div>
              </div>
            </div>
        </div>
        <?php
      }
     ?>
</div>
