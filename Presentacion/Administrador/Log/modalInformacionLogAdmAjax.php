<?php
  $log = new Log($_GET["idlog"]);
  $log -> consultarLogAdm();

  $accion = new Accion($log -> getIdAccion());
  $accion -> consultarNombresAcciones();

  $admin = new Administrador($log -> getIdActor());
  $admin -> consultar();
?>
<div class="container-fluid">
    <?php
      if ($accion -> getNombre() == "Inicio_sesion_Administrador" || $accion -> getNombre() == "Crear_Doctor" ||
          $accion -> getNombre() == "Crear_Evaluador" || $accion -> getNombre() == "Crear_Administrador" ||
          $accion -> getNombre() == "Crear_Especialidad" || $accion -> getNombre() == "Crear_Cliente" ||
          $accion -> getNombre() == "Actualizar_Estado_Cliente" || $accion -> getNombre() == "Actualizar_Estado_Evaluador" ||
          $accion -> getNombre() == "Actualizar_Estado_Administrador" || $accion -> getNombre() == "Actualizar_Estado_Doctor" ||
          $accion -> getNombre() == "Actualizar_Datos_Administrador") {
        ?>
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Informacion del actor</h3>
                    </div>
                    <div class="card-body d-flex justify-content-center">
                        <?php echo  "Cedula: ".$admin -> getCedula()."<br>".
                                    "Nombre: ".$admin -> getNombre()."<br>".
                                    "Apellido: ".$admin -> getApellido()."<br>".
                                    "Correo: ".$admin -> getCorreo()."<br>"?>
                        <img src="<?php echo $admin -> getFoto()?>" width="150px">
                    </div>
                </div>
            </div>
            <div class="col-6">
              <div class="card">
                  <div class="card-header">
                      <h3>Datos de log</h3>
                  </div>
                  <div class="card-body">
                      <?php echo  $log -> getInformacion()?>
                  </div>
              </div>
            </div>
        </div>
        <?php
      }
     ?>
</div>
