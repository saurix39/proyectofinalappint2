<?php
  $log = new Log($_GET["idlog"]);
  $log -> consultarLogDoc();

  $accion = new Accion($log -> getIdAccion());
  $accion -> consultarNombresAcciones();

  $doctor = new Doctor($log -> getIdActor());
  $doctor -> consultar();
?>
<div class="container-fluid">
    <?php
      if ($accion -> getNombre() == "Inicio_sesion_Doctor" || $accion -> getNombre() == "Actualizar_Estado_Cita" || $accion -> getNombre() == "Actualizar_Datos_Doctor") {
        ?>
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Informacion del actor</h3>
                    </div>
                    <div class="card-body d-flex justify-content-center">
                        <?php echo  "Cedula: ".$doctor -> getCedula()."<br>".
                                    "Nombre: ".$doctor -> getNombre()."<br>".
                                    "Apellido: ".$doctor -> getApellido()."<br>".
                                    "Correo: ".$doctor -> getCorreo()."<br>"?>
                        <img src="<?php echo $doctor -> getFoto()?>" width="150px">
                    </div>
                </div>
            </div>
            <div class="col-6">
              <div class="card">
                  <div class="card-header">
                      <h3>Datos de log</h3>
                  </div>
                  <div class="card-body">
                      <?php echo  $log -> getInformacion()?>
                  </div>
              </div>
            </div>
        </div>
        <?php
      }
     ?>
</div>
