<?php
  $log = new Log($_GET["idlog"]);
  $log -> consultarLogCli();

  $accion = new Accion($log -> getIdAccion());
  $accion -> consultarNombresAcciones();

  $cliente = new Cliente($log -> getIdActor());
  $cliente -> consultar();
?>
<div class="container-fluid">
    <?php
      if ($accion -> getNombre() == "Inicio_sesion_Cliente" || $accion -> getNombre() == "Actualizar_Datos_Cliente" || $accion -> getNombre() == "Solicitar_Peticion_Cliente" || $accion -> getNombre() == "Generar_Correccion" || $accion -> getNombre() == "Concretar") {
        ?>
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3>Informacion del actor</h3>
                    </div>
                    <div class="card-body d-flex justify-content-center">
                        <?php echo  "Cedula: ".$cliente -> getCedula()."<br>".
                                    "Nombre: ".$cliente -> getNombre()."<br>".
                                    "Apellido: ".$cliente -> getApellido()."<br>".
                                    "Correo: ".$cliente -> getCorreo()."<br>"?>
                        <img src="<?php echo $cliente -> getFoto()?>" width="150px">
                    </div>
                </div>
            </div>
            <div class="col-6">
              <div class="card">
                  <div class="card-header">
                      <h3>Datos de log</h3>
                  </div>
                  <div class="card-body">
                      <?php echo  $log -> getInformacion()?>
                  </div>
              </div>
            </div>
        </div>
        <?php
      }
     ?>
</div>
