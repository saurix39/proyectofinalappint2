<?php
  $doc = new Doctor("","","","",$_GET["idesp"]);
  $doctores = $doc -> consultarDoctoresEspecialidad();
?>
<table class="table table-responsive-md table-hover table-striped">
  <tr class="thead-dark">
    <th>#</th>
    <th>Cedula</th>
    <th>Correo</th>
    <th>Nombre y Apellido</th>
    <th>Foto</th>

  </tr>
  <?php
  $i=1;
  foreach($doctores as $doc){
      echo "<tr>";
      echo "<td>" . $i . "</td>";
      echo "<td>" . $doc -> getCedula() . "</td>";
      echo "<td>" . $doc -> getCorreo() . "</td>";
      echo "<td>" . $doc -> getNombre()." ".$doc -> getApellido() . "</td>";
      ?>
      <td><img src="<?php echo ($doc -> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$doc -> getFoto() ?>" alt="foto del usuario" width="50px"></td>
      <?php
      echo "</tr>";
      $i++;
  }
  ?>
</table>
