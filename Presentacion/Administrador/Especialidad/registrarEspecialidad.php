<?php
$error=0;
$registrado=false;
if(isset($_POST["registrar"])){
    date_default_timezone_set("America/Bogota");
    $nombre=$_POST["nombre"];
    $doctor=new Doctor();
    if($doctor -> existeEspecialidad($nombre)){
        $error=1;
    }else{
        $doctor -> registrarEspecialidad($nombre);
        $informacion="Se registro una especialidad con la siguiente información:<br><strong>Nombre de la especialidad:</strong> ".$nombre;
        $fecha = date('Y-m-j G-i-s');
        $accion = new Accion("","Crear_Especialidad");
        $accion -> consultarAcciones();

        $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
        $log -> insertarLogAdministrador();
        $registrado=true;
    }
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">
                <div class="card-header text-center">
                    <h2>Registro de especialidad</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Administrador/Especialidad/registrarEspecialidad.php")?>" method="post">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" name="nombre" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="registrar" class="form-control btn btn-secondary">Registrar especialidad</button>
                        </div>
                        <?php if($registrado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Datos ingresados con exito!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php }else if($error==1){ ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                Esta especialidad ya fue registrada!!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
