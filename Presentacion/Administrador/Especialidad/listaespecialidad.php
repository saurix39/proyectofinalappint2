<?php
$esp = new Especialidad();
$cantidad = 5;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $esp -> consultarCantidad($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$especialidadddes = $esp -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>
<div class="container mt-3">
    <div class="row">
        <div class="col-12 col-lg-3">
          <div class="card">
              <div class="card-header">
                  <h3 style="font-family: 'Playfair Display', serif; font-size:30px">Filtro</h3>
              </div>
              <div class="card-body">
                <input class="form-control mr-sm-2" id="buscar" type="search" placeholder="Buscar por nombre" aria-label="Search" data-cantidad="<?php echo $cantidad ?>" value="<?php echo ($filtro != null ? $filtro : "") ?>">
              </div>
          </div>
        </div>
        <div class="col-12 col-lg-9">
          <div class="card">
              <div class="card-header text-white bg-primary">
                  <h4 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de especialidades</h4>
              </div>
              <div class="card-body">
                <div id="contenido">
                    <div class="table table-responsive-sm table-responsive-md">
                      <table class="table table-responsive-md table-hover table-striped">
                        <tr class="thead-dark">
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Ver doctores</th>
                        </tr>
                        <?php
                        $i=1;
                        foreach($especialidadddes as $esp){
                            echo "<tr>";
                            echo "<td>" . $i . "</td>";
                            echo "<td>" . $esp -> getNombre() . "</td>";
                            echo "<td><a id='modal".$esp -> getIdEspecialidad()."' data-toggle='tooltip' data-placement='top' title='Informacion de doctores'><i class='fas fa-info-circle'></i></a></td>";
                            echo "</tr>";
                            $i++;
                        }
                        ?>
                      </table>
                    </div>
                    <div class="d-flex justify-content-end">
                        <nav>
                            <ul class="pagination">
                                <?php if ($pagina > 1) {
                                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Especialidad/listaespecialidad.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                                } ?>
                                <?php for ($i = 1; $i <= $cantPagina; $i++) {
                                    if ($pagina == $i) {
                                        echo "<li class='page-item active'>" .
                                            "<a class='page-link'>$i</a>" .
                                            "</li>";
                                    } else {
                                        echo "<li class='page-item'>" .
                                            "<a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Administrador/Especialidad/listaespecialidad.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                                            "</li>";
                                    }
                                } ?>
                                <?php if ($pagina < $cantPagina) {
                                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Especialidad/listaespecialidad.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                                } ?>
                            </ul>
                        </nav>
                    </div>
                </div>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSup" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Lista de doctores con la especialidad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modalInfo" class="modal-body">
        ...
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
      $("#buscar").keyup(function() {
          if ($(this).val().length >= 1 || $(this).val().length == 0) {
              var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Especialidad/listaespecialidadAjax.php") ?>&filtro=" + $(this).val() + "&cantidad=" + $(this).data("cantidad");
              $("#contenido").load(url);
          }
      });
  });

  $(document).ready(function(){
  <?php
  foreach($especialidadddes as $esp){
  ?>
      $("#modal<?php echo $esp -> getIdEspecialidad()?>").click(function(e){
          var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Especialidad/modalinfoespAjax.php") ?>&idesp=<?php echo $esp -> getIdEspecialidad() ?>";
          $("#modalInfo").load(url);
          $("#modalSup").modal();
      });
  <?php } ?>
  });
</script>
