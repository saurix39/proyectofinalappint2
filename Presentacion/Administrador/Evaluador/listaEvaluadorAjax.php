<?php
$eva = new Evaluador();
$cantidad = 15;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $eva -> consultarCantidad($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$evaluadores = $eva -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>
<div id="contenido">
    <div class="table table-responsive-sm table-responsive-md">
      <table class="table table-responsive-md table-hover table-striped">
        <tr class="thead-dark">
          <th>#</th>
          <th>Cedula</th>
          <th>Correo</th>
          <th>Nombre y Apellido</th>
          <th>Estado</th>
          <th></th>
        </tr>
        <?php
        $i=1;
        foreach($evaluadores as $evas){
            echo "<tr>";
            echo "<td>" . $i . "</td>";
            echo "<td>" . $evas -> getCedula() . "</td>";
            echo "<td>" . $evas -> getCorreo() . "</td>";
            echo "<td>" . $evas -> getNombre()." ".$evas -> getApellido() . "</td>";
        ?>
          <td>
            <select class="custom-select camestado_eva" data-ideva="<?php echo $evas -> getIdEvaluador()?>" style="width: 100px;" >
              <option value="1" <?php echo ($evas->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
              <option value="0" <?php echo ($evas->getEstado() == 0) ? 'selected' : '' ?>>Inactivo</option>
            </select>
          </td>
        <?php
            echo "<td><a id='modal".$evas -> getIdEvaluador()."' data-toggle='tooltip' data-placement='top' title='Informacion del evaluador'><i class='fas fa-info-circle'></i></a></td>";
            echo "</tr>";
            $i++;
        }
        ?>
      </table>
    </div>
    <div class="d-flex justify-content-end">
        <nav>
            <ul class="pagination">
                <?php if ($pagina > 1) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                } ?>
                <?php for ($i = 1; $i <= $cantPagina; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='page-item active'>" .
                            "<a class='page-link'>$i</a>" .
                            "</li>";
                    } else {
                        echo "<li class='page-item'>" .
                            "<a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                            "</li>";
                    }
                } ?>
                <?php if ($pagina < $cantPagina) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Evaluador/listaEvaluador.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                } ?>
            </ul>
        </nav>
    </div>
</div>

<script>
  $(".camestado_eva").change(function() {
      var objJSON = {
          idEva: $(this).data("ideva"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Evaluador/cambioEstadoEvaAjax.php") ?>";

      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });

  $(document).ready(function(){
  <?php
  foreach($evaluadores as $eva){
  ?>
      $("#modal<?php echo $eva -> getIdEvaluador()?>").click(function(e){
          var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Evaluador/modalinfoevaAjax.php") ?>&ideva=<?php echo $eva -> getIdEvaluador() ?>";
          $("#modalInfo").load(url);
          $("#modalSup").modal();
      });
  <?php } ?>
  });
</script>
