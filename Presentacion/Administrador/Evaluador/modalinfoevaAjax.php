<?php
  $eva = new Evaluador($_GET["ideva"]);
  $eva -> consultar();
?>
<div class="container-fluid">
  <div class="d-flex justify-content-center">
          <img src="<?php echo ($eva -> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$eva -> getFoto() ?>" alt="foto del usuario" width="200px">
  </div>
  <table class="table mt-1">
      <tbody>
          <tr>
          <th scope="row">Nombre</th>
          <td><?php echo $eva ->getNombre() ?></td>
          </tr>
          <tr>
          <th scope="row">Apellido</th>
          <td><?php echo $eva ->getApellido() ?></td>
          </tr>
          <tr>
          <th scope="row">Cedula</th>
          <td><?php echo $eva ->getCedula() ?></td>
          </tr>
          <tr>
          <th scope="row">Correo</th>
          <td><?php echo $eva ->getCorreo() ?></td>
          </tr>
          <tr>
          <th scope="row">Fecha de nacimiento</th>
          <td><?php echo $eva ->getFech_nac() ?></td>
          </tr>
      </tbody>
  </table>
</div>
