<?php
  $evaOld = new Evaluador($_POST["idEva"]);
  $evaOld -> consultar();

  date_default_timezone_set('America/Bogota');
  $informacion="Se registro un cambio de estado al evaluador con la siguiente información:<br><strong>Cedula:</strong> ".$evaOld -> getCedula()."<br><strong>Nombre:</strong> ". $evaOld -> getNombre() ."<br><strong>Correo:</strong> ". $evaOld -> getCorreo() ."<br><strong>Estado Anterior:</strong> ". $evaOld -> getEstado();
  $fecha = date('Y-m-j G-i-s');
  $accion = new Accion("","Actualizar_Estado_Evaluador");
  $accion -> consultarAcciones();

  $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
  $log -> insertarLogAdministrador();

  $eva = new Evaluador($_POST["idEva"],"","","","","","",$_POST["estado"]);
  $eva -> actualizar_Estado_Eva();
?>
