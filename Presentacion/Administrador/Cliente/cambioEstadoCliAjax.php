<?php
  $cliOld = new Cliente($_POST["idCli"]);
  $cliOld -> consultar();

  date_default_timezone_set('America/Bogota');
  $informacion="Se registro un cambio de estado al cliente con la siguiente información:<br><strong>Cedula:</strong> ".$cliOld -> getCedula()."<br><strong>Nombre:</strong> ". $cliOld -> getNombre() ."<br><strong>Correo:</strong> ". $cliOld -> getCorreo() ."<br><strong>Estado Anterior:</strong> ". $cliOld -> getEstado();
  $fecha = date('Y-m-j G-i-s');
  $accion = new Accion("","Actualizar_Estado_Cliente");
  $accion -> consultarAcciones();

  $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
  $log -> insertarLogAdministrador();

  $cli = new Cliente($_POST["idCli"],"","","","","","",$_POST["estado"]);
  $cli -> actualizar_Estado_Cli();
?>
