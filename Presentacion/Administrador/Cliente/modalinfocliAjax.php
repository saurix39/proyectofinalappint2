<?php
  $cli = new Cliente($_GET["idcli"]);
  $cli -> consultar();
?>
<div class="container-fluid">
  <div class="d-flex justify-content-center">
          <img src="<?php echo ($cli -> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$cli -> getFoto() ?>" alt="foto del usuario" width="200px">
  </div>
  <table class="table mt-1">
      <tbody>
          <tr>
          <th scope="row">Nombre</th>
          <td><?php echo $cli ->getNombre() ?></td>
          </tr>
          <tr>
          <th scope="row">Apellido</th>
          <td><?php echo $cli ->getApellido() ?></td>
          </tr>
          <tr>
          <th scope="row">Cedula</th>
          <td><?php echo $cli ->getCedula() ?></td>
          </tr>
          <tr>
          <th scope="row">Correo</th>
          <td><?php echo $cli ->getCorreo() ?></td>
          </tr>
          <tr>
          <th scope="row">Fecha de nacimiento</th>
          <td><?php echo $cli ->getFech_nac() ?></td>
          </tr>
      </tbody>
  </table>
</div>
