<?php
$adm = new Administrador();
$cantidad = 15;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $adm -> consultarCantidad($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$administradores = $adm -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>

<div id="contenido">
    <div class="table table-responsive-sm table-responsive-md">
      <table class="table table-responsive-md table-hover table-striped">
        <tr class="thead-dark">
          <th>#</th>
          <th>Cedula</th>
          <th>Correo</th>
          <th>Nombre y Apellido</th>
          <th>Estado</th>
          <th></th>
        </tr>
        <?php
        $i=1;
        foreach($administradores as $adms){
            echo "<tr>";
            echo "<td>" . $i . "</td>";
            echo "<td>" . $adms -> getCedula() . "</td>";
            echo "<td>" . $adms -> getCorreo() . "</td>";
            echo "<td>" . $adms -> getNombre()." ".$adms -> getApellido() . "</td>";
        ?>
          <td>
            <select class="custom-select camestado_adm" data-idadm="<?php echo $adms -> getIdAdministrador()?>" style="width: 100px;" >
              <option value="1" <?php echo ($adms->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
              <option value="0" <?php echo ($adms->getEstado() == 0) ? 'selected' : '' ?>>Inactivo</option>
            </select>
          </td>
        <?php
            echo "<td><a id='modal".$adms -> getIdAdministrador()."' data-toggle='tooltip' data-placement='top' title='Informacion del administrador'><i class='fas fa-info-circle'></i></a></td>";
            echo "</tr>";
            $i++;
        }
        ?>
      </table>
    </div>
    <div class="d-flex justify-content-end">
        <nav>
            <ul class="pagination">
                <?php if ($pagina > 1) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Administradores/listaAdministradores.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                } ?>
                <?php for ($i = 1; $i <= $cantPagina; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='page-item active'>" .
                            "<a class='page-link'>$i</a>" .
                            "</li>";
                    } else {
                        echo "<li class='page-item'>" .
                            "<a class='page-link' href='index.php?pid=" . base64_encode("Presentacion/Administrador/Administradores/listaAdministradores.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                            "</li>";
                    }
                } ?>
                <?php if ($pagina < $cantPagina) {
                    echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("Presentacion/Administrador/Administradores/listaAdministradores.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                } ?>
            </ul>
        </nav>
    </div>
</div>

<script>
  $(".camestado_adm").change(function() {
      var objJSON = {
          idAdm: $(this).data("idadm"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Administradores/cambioEstadoAdmAjax.php") ?>";

      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });

  $(document).ready(function(){
  <?php
  foreach($administradores as $adm){
  ?>
      $("#modal<?php echo $adm -> getIdAdministrador()?>").click(function(e){
          var url = "indexAjax.php?pid=<?php echo base64_encode("Presentacion/Administrador/Administradores/modalinfoadmAjax.php") ?>&idadm=<?php echo $adm -> getIdAdministrador() ?>";
          $("#modalInfo").load(url);
          $("#modalSup").modal();
      });
  <?php } ?>
  });
</script>
