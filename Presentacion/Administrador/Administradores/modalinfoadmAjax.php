<?php
  $admin = new Administrador($_GET["idadm"]);
  $admin -> consultar();
?>
<div class="container-fluid">
  <div class="d-flex justify-content-center">
          <img src="<?php echo ($admin -> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$admin -> getFoto() ?>" alt="foto del usuario" width="200px">
  </div>
  <table class="table mt-1">
      <tbody>
          <tr>
          <th scope="row">Nombre</th>
          <td><?php echo $admin ->getNombre() ?></td>
          </tr>
          <tr>
          <th scope="row">Apellido</th>
          <td><?php echo $admin ->getApellido() ?></td>
          </tr>
          <tr>
          <th scope="row">Cedula</th>
          <td><?php echo $admin ->getCedula() ?></td>
          </tr>
          <tr>
          <th scope="row">Correo</th>
          <td><?php echo $admin ->getCorreo() ?></td>
          </tr>
          <tr>
          <th scope="row">Fecha de nacimiento</th>
          <td><?php echo $admin ->getFech_nac() ?></td>
          </tr>
      </tbody>
  </table>
</div>
