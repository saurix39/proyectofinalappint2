<?php
  $admOld = new Administrador($_POST["idAdm"]);
  $admOld -> consultar();

  date_default_timezone_set('America/Bogota');
  $informacion="Se registro un cambio de estado al administrador  con la siguiente información:<br><strong>Cedula:</strong> ".$admOld -> getCedula()."<br><strong>Nombre:</strong> ". $admOld -> getNombre() ."<br><strong>Correo:</strong> ". $admOld -> getCorreo() ."<br><strong>Estado Anterior:</strong> ". $admOld -> getEstado();
  $fecha = date('Y-m-j G-i-s');
  $accion = new Accion("","Actualizar_Estado_Administrador");
  $accion -> consultarAcciones();

  $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
  $log -> insertarLogAdministrador();

  $adm = new Administrador($_POST["idAdm"],"","","","","","",$_POST["estado"]);
  $adm -> actualizar_Estado_Adm();

?>
