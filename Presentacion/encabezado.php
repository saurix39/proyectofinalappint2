<div class="container">
    <div class="row d-flex justify-content-between">
        <div class="col-md-4 col-12 d-none d-lg-block mt-1" >
            <img src="Img/Logo.jpg" class="img-fluid" alt="Logo de la empresa" width="150px" >
        </div>
        <div class="col-md-4 col-12 text-center mt-3">
            <p style="font-family: 'Abril Fatface', monospace; font-size:40px">Quick Medicine</p>
        </div>
        <div class="col-md-4 col-12 mt-1">
            <div  class="d-flex justify-content-center">
                <i class="fab fa-instagram" style="font-size:400%; text-align:center; margin: 5px;"></i>
                <i class="fab fa-facebook-square" style="font-size:400%; text-align:center;margin: 5px;"></i>
                <i class="fab fa-youtube" style="font-size:400%; text-align:center;margin: 5px;"></i>
            </div>
        </div>
    </div>
</div>
