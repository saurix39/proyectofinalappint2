<?php
$peticion =new Peticion($_GET["peticion"]);
$peticion -> consultar();
$observacion=new Observacion("",$peticion -> getIdPeticion());
$observaciones = $observacion -> observaciones();
$edicionPet= new EdicionPeticion("",$_GET["peticion"]);
$edicionesPet=$edicionPet -> edicionesPet();
if(isset($_POST["negar"])){
    $peticion = new Peticion($_GET["peticion"]);
    $peticion -> negar();
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $acpeticion=new AcPeticiones("",$_GET["peticion"],$_SESSION["id"],"2",$fecha);
    $acpeticion -> insertar();

    $informacion="Se nego una peticion con la siguiente información:<br><strong>N° Peticion:</strong> ".$_GET["peticion"]."<br><strong>Identificacion del evaluador:</strong> ".$_SESSION["id"]."<br><strong>Fecha:</strong> ".$fecha;
    $fecha = date('Y-m-j G-i-s');
    $accion = new Accion("","Negar");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
    $log -> insertarLogEvaluador();

    header("Location: index.php?pid=".base64_encode("Presentacion/Evaluador/sesionEvaluador.php")."&ac=negada");
}else if(isset($_POST["correccion"])){
    header("Location: index.php?pid=".base64_encode("Presentacion/Evaluador/generarObservacion.php")."&peticion=".$_GET["peticion"]);
}else if(isset($_POST["aceptar"])){
    $peticion = new Peticion($_GET["peticion"]);
    $peticion -> aceptar();
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $acpeticion=new AcPeticiones("",$_GET["peticion"],$_SESSION["id"],"1",$fecha);
    $acpeticion -> insertar();

    $informacion="Se acepto una peticion con la siguiente información:<br><strong>N° Peticion:</strong> ".$_GET["peticion"]."<br><strong>Identificacion del evaluador:</strong> ".$_SESSION["id"]."<br><strong>Fecha:</strong> ".$fecha;
    $fechaLog = date('Y-m-j G-i-s');
    $accion = new Accion("","Aceptar");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fechaLog);
    $log -> insertarLogEvaluador();

    header("Location: index.php?pid=".base64_encode("Presentacion/Evaluador/sesionEvaluador.php")."&ac=aceptada");
}
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Gestion de peticion</h2>
                </div>
                <div class="card-body">
                <div class="row">
                    <div class="col-4">
                    </div>
                    <div class="col-md-4 col-12">
                        <img src="<?php echo $peticion -> getId_cliente_fk() -> getFoto() ?>" alt="Imagen del cliente" width="100%" class="rounded">
                    </div>
                </div>
                <div class="row">
                    <div class="col mt-3">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="inputEmail4">Identificacion de la petición</label>
                                <input type="text" class="form-control" id="inputEmail4" disabled value="<?php echo $peticion -> getIdPeticion() ?>">
                                </div>
                                <div class="form-group col-md-6">
                                <label for="inputPassword4">Fecha de solicitud</label>
                                <input type="text" class="form-control" id="inputPassword4" disabled value="<?php echo $peticion -> getFecha() ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="inputEmail4">Nombre del cliente</label>
                                <input type="text" class="form-control" id="inputEmail4" disabled value="<?php echo $peticion -> getId_cliente_fk() -> getNombre() ?>">
                                </div>
                                <div class="form-group col-md-6">
                                <label for="inputPassword4">Apellido del cliente</label>
                                <input type="text" class="form-control" id="inputPassword4" disabled value="<?php echo $peticion -> getId_cliente_fk() -> getApellido() ?>">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="inputEmail4">Cedula del cliente</label>
                                <input type="text" class="form-control" id="inputEmail4" disabled value="<?php echo $peticion -> getId_cliente_fk() -> getCedula() ?>">
                                </div>
                                <div class="form-group col-md-6">
                                <label for="inputPassword4">Fecha de nacimiento del cliente</label>
                                <input type="text" class="form-control" id="inputPassword4" disabled value="<?php echo $peticion -> getId_cliente_fk() -> getFech_nac() ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label >Correo del cliente</label>
                                <input type="text" class="form-control" id="inputAddress" disabled value="<?php echo $peticion -> getId_cliente_fk() -> getCorreo() ?>">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2">Especialidad</label>
                                <input type="text" class="form-control" id="inputAddress2" disabled value="<?php echo $peticion -> getEspecialidad() ?>">
                            </div>
                            <div class="form-group">
                                <label >Descripción</label>
                                <textarea class="form-control" name="descripcion" rows="5" disabled ><?php echo $peticion -> getInformacion() ?></textarea>
                            </div>
                            <div class="form-group">
                                <label >Remision</label>
                                <img src="<?php echo $peticion -> getRemision()?>" alt="Imagen de remision" width="100%" class="rounded">
                            </div>
                            <?php
                                if(count($observaciones)>=1){
                                    echo "<label class='font-weight-bold'>Observaciones</label>";
                                    echo "<br>";
                                    for($i=0;$i<count($observaciones);$i++){
                                        echo "<label class='mt-1'>Evaluador - ". $observaciones[$i] -> getFecha() ."</label>";
                                        echo "<textarea class='form-control'  rows='3' disabled >". $observaciones[$i] -> getInformacion() ."</textarea>";
                                        echo (($_SESSION["rol"]=="Evaluador")?"<a href='index.php?pid=". base64_encode("Presentacion/Evaluador/generarEdicion.php") ."&observacion=". $observaciones[$i] -> getIdObservacion() ."'>Editar</a>":"");
                                        echo "<br>";
                                        $edicion=new Edicion("", $observaciones[$i] -> getIdObservacion());
                                        $ediciones=$edicion -> ediciones();
                                        if(count($ediciones)>=1){
                                            echo "<label class='font-weight-bold'>Ediciones</label>";
                                            echo "<br>";
                                            foreach($ediciones as $ed){
                                                echo "<label>Evaluador - ". $ed -> getFecha() ."</label>";
                                                echo "<textarea class='form-control'  rows='3' disabled >". $ed-> getInformacion() ."</textarea>";
                                            }
                                        }
                                        if(isset($edicionesPet[$i])){
                                            echo "<label class='font-weight-bold'>Corrección ". ($i+1) ." - Cliente - ". $edicionesPet[$i] -> getFecha() ."</label>";
                                            echo "<br>";
                                            echo "<label >Descripción</label>";
                                            echo "<textarea class='form-control'  rows='3' disabled >". $edicionesPet[$i] -> getInformacion() ."</textarea>";
                                            echo "<label >Remision</label>";
                                            echo "<br>";
                                            echo "<img src='" . $edicionesPet[$i] -> getRemision() . "' alt='Imagen de remision' width='100%' class='rounded'>";
                                        }
                                    }
                                }
                                if($peticion -> getEstado()=="-1"){
                                    $acpet=new AcPeticiones("",$peticion -> getIdPeticion(),"","2","");
                                    $acpet -> consultar();
                                    echo "<center><h2 class='mt-2'>La peticion fue negada el ". $acpet -> getFecha() ."</h2></center>";
                                }else if($peticion -> getEstado()=="1"){
                                    $acpet=new AcPeticiones("",$peticion -> getIdPeticion(),"","1","");
                                    $acpet -> consultar();
                                    echo "<center><h2>La peticion fue aceptada el ". $acpet -> getFecha() ."</h2></center>";
                                }else if($peticion -> getEstado()=="3"){
                                    $acpet=new AcPeticiones("",$peticion -> getIdPeticion(),"","1","");
                                    $acpet -> consultar();
                                    echo "<center><h2>La petición fue aceptada el ". $acpet -> getFecha() ."</h2></center>";
                                    $acpeti=new AcPeticionesCli("",$peticion -> getIdPeticion(),"","3","");
                                    $acpeti -> consultar();
                                    echo "<center><h3>La petición fue concretada el ". $acpeti -> getFecha() ."</h3></center>";
                                }
                            ?>
                        </form>
                        <?php if($_SESSION["rol"]=="Evaluador"){ ?>
                            <form action="index.php?pid=<?php echo base64_encode("Presentacion/Evaluador/gestionarPeticion.php") ?>&peticion=<?php echo $peticion -> getIdPeticion() ?>" method="post">
                                    <div class="row mt-3">
                                        <div class="col-4">
                                            <button type="submit" name="negar" class="btn btn-danger btn-block" <?php echo (($peticion -> getEstado()=='-2' || $peticion -> getEstado()=='-1' || $peticion -> getEstado()=='1' || $peticion -> getEstado()=='3')?"disabled":"") ?>> Negar petición</button>
                                        </div>
                                        <div class="col-4">
                                            <button type="submit" name="correccion" class="btn btn-primary btn-block" <?php echo (($peticion -> getEstado()=='-2' || $peticion -> getEstado()=='-1' || $peticion -> getEstado()=='1' || $peticion -> getEstado()=='3')?"disabled":"") ?>>Generar observación</button>
                                        </div>
                                        <div class="col-4">
                                            <button type="submit" name="aceptar" class="btn btn-success btn-block" <?php echo (($peticion -> getEstado()=='-2' || $peticion -> getEstado()=='-1' || $peticion -> getEstado()=='1' || $peticion -> getEstado()=='3')?"disabled":"") ?>>Aceptar petición</button>
                                        </div>
                                    </div>
                            </form>
                        <?php } else if($_SESSION["rol"]=="Cliente"){ ?>
                            <div class="row">
                                <div class="col-6 mt-3">
                                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/generarCorreccion.php") ?>&peticion=<?php echo $peticion -> getIdPeticion() ?>" method="post">
                                        <button type="submit" name="corregir" class="btn btn-primary btn-block" <?php echo (($peticion -> getEstado()=='-2')?"":"disabled") ?>> Generar corrección</button>
                                    </form>
                                </div>
                                <div class="col-6 mt-3">
                                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Cliente/concretar.php") ?>&peticion=<?php echo $peticion -> getIdPeticion() ?>" method="post">
                                    <button type="submit" name="concretar" class="btn btn-success btn-block" <?php echo (($peticion -> getEstado()=='1')?"":"disabled") ?>>Concretar</button>
                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
