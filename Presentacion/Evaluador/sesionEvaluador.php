<?php
$evaluador= new Evaluador($_SESSION["id"]);
$evaluador -> consultar();
$ac="";
if(isset($_GET["ac"])){
$ac=$_GET["ac"];
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-secondary mt-1">  
                <div class="d-flex justify-content-center">
                        <img src="<?php echo ($evaluador-> getFoto()=="")?"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png":$evaluador -> getFoto() ?>" alt="foto del usuario" width="30%">
                </div>
                <table class="table mt-1">
                    <tbody>
                        <tr>
                        <th scope="row">Nombre</th>
                        <td><?php echo $evaluador ->getNombre() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Apellido</th>
                        <td><?php echo $evaluador ->getApellido() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Cedula</th>
                        <td><?php echo $evaluador ->getCedula() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Correo</th>
                        <td><?php echo $evaluador ->getCorreo() ?></td>
                        </tr>
                        <tr>
                        <th scope="row">Fecha de nacimiento</th>
                        <td><?php echo $evaluador ->getFech_nac() ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <?php if($ac=="negada"){ ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    La peticion fue negada!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php }else if($ac=="aceptada"){ ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    La peticion fue aceptada!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php }else if($ac=="enObs"){ ?>
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    La peticion fue puesta en corrección!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>     
        </div>
    </div>
</div>