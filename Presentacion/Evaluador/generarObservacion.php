<?php
$peticion="";
if(isset($_GET["peticion"])){
$peticion=$_GET["peticion"];
}
if(isset($_POST["generar"])){
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $observacion=new Observacion("",$peticion,$_POST["observacion"],$fecha);
    $observacion -> registrarObservacion();
    $peticion_ = new Peticion($peticion);
    $peticion_ -> enObser();

    $informacion="Se genera una observacion a una peticion con la siguiente información:<br><strong>N° Peticion:</strong> ".$peticion."<br><strong>Identificacion del evaluador:</strong> ".$_SESSION["id"]."<br><strong>Observacion:</strong> ".$_POST["observacion"];
    $fecha = date('Y-m-j G-i-s');
    $accion = new Accion("","Generar_Observacion");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fecha);
    $log -> insertarLogEvaluador();

    header("Location: index.php?pid=".base64_encode("Presentacion/Evaluador/sesionEvaluador.php")."&ac=enObs");
}
?>
<div class="container">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Formulario de observación</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Evaluador/generarObservacion.php")?>&peticion=<?php echo $peticion?>" method="post">
                        <div class="form-group">
                            <label >Observación</label>
                            <textarea class="form-control" name="observacion" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="generar" class="form-control btn btn-secondary">Generar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
