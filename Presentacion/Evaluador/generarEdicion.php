<?php
$editado=false;
$observacion="";
if(isset($_GET["observacion"])){
    $observacion=$_GET["observacion"];
}
if(isset($_POST["editar"])){
    date_default_timezone_set("America/Bogota");
    $fecha=date('Y-m-j G-i-s');
    $edicion=new Edicion("",$observacion,$_POST["descripcion"],$fecha);
    $edicion -> registrarEdicion();

    $informacion="Se genera una edicion a una observacion con la siguiente información:<br><strong>N° Observacion:</strong> ".$observacion."<br><strong>Identificacion del evaluador:</strong> ".$_SESSION["id"]."<br><strong>Nueva observacion:</strong> ".$_POST["descripcion"];
    $fechalog = date('Y-m-j G-i-s');
    $accion = new Accion("","Generar_Edicion");
    $accion -> consultarAcciones();

    $log = new Log("",$accion -> getIdAccion(),$_SESSION["id"],$informacion,$fechalog);
    $log -> insertarLogEvaluador();

    $editado=true;
}
?>
<div class="container">
    <div class="row">
        <div class="col-2">
        </div>
        <div class="col-md-8 col-12">
            <div class="card border border-dark mt-1">
                <div class="card-header text-center">
                    <h2>Edicion de observación</h2>
                </div>
                <div class="card-body">
                    <form action="index.php?pid=<?php echo base64_encode("Presentacion/Evaluador/generarEdicion.php")?>&observacion=<?php echo $observacion?>" method="post">
                        <div class="form-group">
                            <label >Edición</label>
                            <textarea class="form-control" name="descripcion" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="editar" class="form-control btn btn-secondary">Realizar edición</button>
                        </div>
                        <?php if($editado){?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Su edición fue registrada
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
