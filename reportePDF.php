<?php
require "fpdf/fpdf.php";
require "Logica/Cita.php";
require "Logica/Peticion.php";
require "Logica/Evaluador.php";
require "Logica/Observacion.php";
require "Logica/Edicion.php";
require "Logica/Doctor.php";
require "Logica/EdicionPeticion.php";
require "Logica/AcPeticiones.php";
require "Logica/AcPeticionesCli.php";
require "Logica/Horario.php";

$idcita="";
if(isset($_GET["cita"])){
$idcita=$_GET["cita"];
}
$cita = new Cita($idcita);
$cita -> consultar();
$peticion = new Peticion($cita -> getId_peticion_fk());
$peticion -> consultar();
$observacion=new Observacion("",$peticion -> getIdPeticion());
$observaciones = $observacion -> observaciones();
$edicionPet= new EdicionPeticion("",$peticion -> getIdPeticion());
$edicionesPet=$edicionPet -> edicionesPet();
$proceso =new FPDF("P","mm","Letter");
$proceso -> SetFont("times", "B", 40);
$proceso -> AddPage();
$proceso -> Cell(40,40,$proceso -> Image("Img/logo-pequeño.jpg",$proceso -> getX(), $proceso -> getY(),40,40),0,0,"C");
$proceso -> Cell(160, 23, "Quick Medicine", 0, 2, "C");
$proceso -> SetFont("times", "I", 17);
$proceso -> Cell(160, 10, "Medicina virtual", 0, 1, "C");
$proceso -> SetFont("times", "B", 30);
$proceso -> Cell(195, 23, "Reporte del proceso de peticion de cita", 0, 1, "C");
$proceso -> Cell(60, 55, " ", 0, 0, "C");
$proceso -> Cell(75, 55, $proceso -> Image($peticion -> getId_cliente_fk() -> getFoto() ,$proceso -> getX(), $proceso -> getY(),75,55), 0, 0, "C");
$proceso -> Cell(60, 55, " ", 0, 1, "C");
$proceso -> Cell(195, 3, "", 0, 1, "C");
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(97, 10, "Identificacion de la peticion", 0, 0, "C");
$proceso -> Cell(1, 10, "", 0, 0, "C");
$proceso -> Cell(97, 10, "Fecha y hora de solicitud", 0, 1, "C");
$proceso -> SetFont("times", "", 12);
$proceso -> Cell(97, 8, $peticion -> getIdPeticion() , 1, 0, "C");
$proceso -> Cell(1, 8, "", 0, 0, "C");
$proceso -> Cell(97, 8, $peticion -> getFecha() , 1, 1, "C");
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(97, 10, "Nombre del cliente", 0, 0, "C");
$proceso -> Cell(1, 10, "", 0, 0, "C");
$proceso -> Cell(97, 10, "Apellido del cliente", 0, 1, "C");
$proceso -> SetFont("times", "", 12);
$proceso -> Cell(97, 8, $peticion -> getId_cliente_fk() -> getNombre() , 1, 0, "C");
$proceso -> Cell(1, 8, "", 0, 0, "C");
$proceso -> Cell(97, 8, $peticion -> getId_cliente_fk() -> getApellido() , 1, 1, "C");
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(97, 10, "Cedula del cliente", 0, 0, "C");
$proceso -> Cell(1, 10, "", 0, 0, "C");
$proceso -> Cell(97, 10, "Fecha de nacimiento del cliente", 0, 1, "C");
$proceso -> SetFont("times", "", 12);
$proceso -> Cell(97, 8, $peticion -> getId_cliente_fk() -> getCedula() , 1, 0, "C");
$proceso -> Cell(1, 8, "", 0, 0, "C");
$proceso -> Cell(97, 8, $peticion -> getId_cliente_fk() -> getFech_nac() , 1, 1, "C");
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(195, 10, "Correo del cliente", 0, 1, "C");
$proceso -> SetFont("times", "", 12);
$proceso -> Cell(195, 8, $peticion -> getId_cliente_fk() -> getCorreo() , 1, 1, "C");
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(195, 10, "Especialidad a la que se solicita la cita", 0, 1, "C");
$proceso -> SetFont("times", "", 12);
$proceso -> Cell(195, 8, $peticion -> getEspecialidad() , 1, 1, "C");
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(195, 10, "Descripcion de la peticion", 0, 1, "C");
$proceso -> SetFont("times", "", 12);
$proceso -> MultiCell(195,7,$peticion -> getInformacion() ,1,"L");
$proceso -> AddPage();
$proceso -> SetFont("times", "B", 15);
$proceso -> Cell(195, 10, "Remision", 0, 1, "C");
$proceso -> setX(0);
$proceso -> Image($peticion -> getRemision() ,$proceso -> getX(), $proceso -> getY(),216,260);
$proceso -> AddPage();
if(count($observaciones)>=1){
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "Observaciones", 0, 1, "L");
    for($i=0;$i<count($observaciones);$i++){
        $proceso -> SetFont("times", "B", 15);
        $proceso -> Cell(195, 10, "Evaluador - ".$observaciones[$i] -> getFecha(), 0, 1, "L");
        $proceso -> SetFont("times", "", 12);
        $proceso -> MultiCell(195,7, $observaciones[$i] -> getInformacion() ,1,"L");
        $edicion=new Edicion("", $observaciones[$i] -> getIdObservacion());
        $ediciones=$edicion -> ediciones();
        if(count($ediciones)>=1){
            $proceso -> SetFont("times", "B", 15);
            $proceso -> Cell(195, 10, "Ediciones", 0, 1, "L");
            foreach($ediciones as $ed){
                $proceso -> SetFont("times", "B", 15);
                $proceso -> Cell(10, 10, "", 0, 0, "L");
                $proceso -> Cell(185, 10, "Evaluador - ". $ed -> getFecha(), 0, 1, "L");
                $proceso -> SetFont("times", "", 12);
                $proceso -> Cell(10, 10, "", 0, 0, "L");
                $proceso -> MultiCell(185,7, $ed-> getInformacion() ,1,"L");
            }
        }
        if(isset($edicionesPet[$i])){
            $proceso -> SetFont("times", "B", 15);
            $proceso -> Cell(195, 10, "Correccion ". ($i+1) ." - Cliente - ". $edicionesPet[$i] -> getFecha(), 0, 1, "L");
            $proceso -> Cell(195, 10, "Descripcion", 0, 1, "L");
            $proceso -> SetFont("times", "", 12);
            $proceso -> MultiCell(195,7, $edicionesPet[$i] -> getInformacion() ,1,"L");
            $proceso -> AddPage();
            $proceso -> SetFont("times", "B", 15);
            $proceso -> Cell(195, 10, "Remision", 0, 1, "C");
            $proceso -> setX(0);
            $proceso -> Image($edicionesPet[$i] -> getRemision() ,$proceso -> getX(), $proceso -> getY(),216,260);
            $proceso -> AddPage();
        }
    }
}
if($peticion -> getEstado()=="-1"){
    $acpet=new AcPeticiones("",$peticion -> getIdPeticion(),"","2","");
    $acpet -> consultar();
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "La peticion fue negada el ". $acpet -> getFecha(), 0, 1, "C");
}else if($peticion -> getEstado()=="1"){
    $acpet=new AcPeticiones("",$peticion -> getIdPeticion(),"","1","");
    $acpet -> consultar();
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "La peticion fue aceptada el ". $acpet -> getFecha(), 0, 1, "C");
}else if($peticion -> getEstado()=="3"){
    $acpet=new AcPeticiones("",$peticion -> getIdPeticion(),"","1","");
    $acpet -> consultar();
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "La peticion fue aceptada el ". $acpet -> getFecha(), 0, 1, "C");
    $acpeti=new AcPeticionesCli("",$peticion -> getIdPeticion(),"","3","");
    $acpeti -> consultar();
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "La peticion fue concretada el ". $acpeti -> getFecha() , 0, 1, "C");
    $docpdf = new Doctor($cita -> getId_doctor_fk());
    $docpdf -> consultar();
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "Doctor", 0, 1, "C");
    $proceso -> Cell(60, 55, " ", 0, 0, "C");
    $proceso -> Cell(75, 55, $proceso -> Image((($docpdf -> getFoto()!="")?$docpdf -> getFoto():"https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-2/256/man-icon.png") ,$proceso -> getX(), $proceso -> getY(),75,55), 0, 0, "C");
    $proceso -> Cell(195, 3, "", 0, 1, "C");
    $proceso -> Cell(60, 55, " ", 0, 1, "C");
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(97, 10, "Nombre del doctor", 0, 0, "C");
    $proceso -> Cell(1, 10, "", 0, 0, "C");
    $proceso -> Cell(97, 10, "Apellido del doctor", 0, 1, "C");
    $proceso -> SetFont("times", "", 12);
    $proceso -> Cell(97, 8, $docpdf -> getNombre() , 1, 0, "C");
    $proceso -> Cell(1, 8, "", 0, 0, "C");
    $proceso -> Cell(97, 8, $docpdf -> getApellido() , 1, 1, "C");
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(195, 10, "Fecha de la cita", 0, 1, "C");
    $proceso -> SetFont("times", "", 12);
    $proceso -> Cell(195, 8, $cita -> getFecha() , 1, 1, "C");
    $turno = new Horario($cita -> getId_horario_fk());
    $turno -> consultar();
    $proceso -> SetFont("times", "B", 15);
    $proceso -> Cell(97, 10, "Hora de inicio", 0, 0, "C");
    $proceso -> Cell(1, 10, "", 0, 0, "C");
    $proceso -> Cell(97, 10, "Hora final", 0, 1, "C");
    $proceso -> SetFont("times", "", 12);
    $proceso -> Cell(97, 8, $turno -> getHora_inicio() , 1, 0, "C");
    $proceso -> Cell(1, 8, "", 0, 0, "C");
    $proceso -> Cell(97, 8, $turno -> getHora_final() , 1, 1, "C");
}
$proceso -> Output();
?>
